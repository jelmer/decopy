#!/usr/bin/env python3
# -*- coding: utf-8 -*- vim60:fdm=marker
#
# Copyright: 2018, Maximiliano Curia <maxy@debian.org>
#
# License: ISC
#  Permission to use, copy, modify, and/or distribute this software for any
#  purpose with or without fee is hereby granted, provided that the above
#  copyright notice and this permission notice appear in all copies.
#  .
#  THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH
#  REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
#  AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY SPECIAL, DIRECT,
#  INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
#  LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
#  OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
#  PERFORMANCE OF THIS SOFTWARE.

''' Regular expressions collections used by the matchers. '''

try:
    import regex as re
except ImportError:
    import re
# import the stdlib re, as re.sub is more efficient than regex.sub
import re as reo

from functools import partial

from .datatypes import ReSub, ReFlags, ReLicense


# LICENSES_RES common get_details
def _name_version(text, version_match, group, name=None):
    if not name:
        name = group
    version = version_match.group(1)
    return '{}{}'.format(
        name,
        '-{}'.format(version.rstrip('.0')
                     if version else '')
    )


# subs are way more efficient when using python's re (reo here)
COMMENTS_SUBS = (
    ReSub(reo.compile(
        # Fortran (fortran is weird: https://gcc.gnu.org/ml/gcc/2000-01/msg00250.html)
        r'^C[ \t]'
        # C / C++ (avoid dropping :// from uris)
        r'|(?<!:)//+'
        # m4
        r'|^dnl\s'
        # Generic
        r'|^[\s\W]+\s',
        re.I | re.MULTILINE), r' '),
    # Non text (and compress white)
    ReSub(reo.compile(r'[^ \w\'.,@:;()/+-]'), r' '),
    ReSub(reo.compile(r' {2,}'), r' '),
)

COPYRIGHT_PRE_IGNORE_REGEXES = (
    # #define foo(c) -- not copyright
    ReFlags(r'^#\s*define\s+.*\([^)]*c[^)]*\)'),
    # po files data
    ReFlags(r'^msgstr\s'),
    ReFlags(r'^#.{,2} msgstr\s'),
    ReFlags(r'^msgid\s'),
    ReFlags(r'^#.{,2} msgid\s'),
    # mostly gpl stuff
    ReFlags(
        r'We\sprotect\syour\srights\swith\s'
        r'(?:two\ssteps:|a\stwo-step\smethod:)\s\(1\)\s(?:we\s)?copyright'),
    ReFlags(r'and\sput\sthe\sfollowing\scopyright'),
    ReFlags(r'connection\swith\sthe\scopyright'),
    ReFlags(
        r'applicable\scopyright\slaw\sincludes'),
    ReFlags(
        r'Original\scopyright\sfollows'),
    ReFlags(
        r'compilation\sand\sits\sresulting\scopyright'),
    ReFlags(
        r'under\sapplicable\scopyright\slaw'),
    ReFlags(
        r'copyright\son\sthe\sProgram,\sand'),
    ReFlags(
        r'copyright\son\sthe\ssoftware,\sand'),
    ReFlags(
        r'can\sgive\sappropriate\scopyright\spermission'),
    ReFlags(
        r'requiring\scopyright\spermission'),
    ReFlags(
        r'copyright\streaty\sadopted\son'),
    ReFlags(
        r'copyright\sby\stheir\scontributors'),
    ReFlags(
        r'the\ same\ copyright\ terms'),
    ReFlags(
        r'for\ copyright\ years'),
    ReFlags(
        r'Added\ copyright\ notes'),
    ReFlags(
        r'adjust\ copyright'),
    ReFlags(
        r'use\ copyright\ ranges'),
    ReFlags(
        r'copyright\ does(?:\ _?NOT_?|n\'t)'),
    ReFlags(
        r'copyright\ unless'),
    ReFlags(
        r'sign a "?copyright disclaimer"?'),
    # FSF address
    ReFlags(
        r'59\ Temple\ Place,\ Suite\ 330,\ Boston,\ MA\ \ ?02111-1307,?\ \ ?USA',
        re.I),
    ReFlags(
        r'51\ Franklin\ St(?:reet)?,\ Fifth\ Floor,\ Boston,\ MA\ \ ?02110-1301,?\ \ ?USA'
    ),
    # partial fsf address as found in kcoreaddons
    ReFlags(
        r'\bBoston,\ MA\ \ ?02110-\d+,?\ \ ?USA'
    ),
    # public domain
    ReFlags(
        r'disclaims\ copyright'),
    # creative commons
    ReFlags(
        r'the\ applicable\ copyright'),
    ReFlags(
        r'under\ copyright\ law\ or'),
    ReFlags(
        r'free\ from\ copyright\ or'),
    ReFlags(
        r'WIPO\ Copyright\ Treaty'),
    # Academic
    ReFlags(
        r'requirements\ and\ penalties\ of\ the\ U\.S\.\ Copyright'),
    ReFlags(
        r'Grant\ of\ Copyright\ License\.'),
    ReFlags(
        r'prohibited\ by\ U\.S\.\ copyright\ law'),
    # Artistic
    ReFlags(
        r'under\ the\ copyright\ of\ this\ Package'),
    # MPL-1.1
    ReFlags(
        r'\(c\)\ +Representations'),
    # MPL-2.0
    ReFlags(
        r'copyright\ doctrines\ of\ fair\ use'),
    # Ms-PL
    ReFlags(
        r'Copyright\ Grant.?\ Subject\ to'),
    # Ms-PL
    ReFlags(
        r'copyright\ license\ to'),
    # OpenLDAP 2.8
    ReFlags(
        r'copyright\ in\ this\ Software'),
    # djvulibre d/copyright
    ReFlags(
        r'COPYRIGHT\ at\ the\ top\ level'),
    # found in abiword
    ReFlags(
        r'Trademark\ and\ copyright\ issues\ are'),
    # found in decopy
    ReFlags(
        r'group_by\ copyright'),
    ReFlags(
        r'copyright\ file'),
    ReFlags(
        r'copyright\ attribution'),
    ReFlags(
        r'copyright\ format'),
    ReFlags(
        r'copyright\ helper'),
    ReFlags(
        r'copyright\ detection'),
    # found in krita code
    ReFlags(
        r'Copyright\sflag'),
    # found in plan code
    ReFlags(
        r'{\scopyright\s=\sc;\s}'),
    # krita gmic
    ReFlags(
        r'Quick\scopyright\s:\sgimp_quick_copyright'),
    # found in muffin/nemo/cinnamon-c-c
    ReFlags(
        r'\bcopyright\s(?:info|headers?|lines?|messages?|reasons?|symbols?)\b',
        re.I),
    # found in muffin
    ReFlags(
        r'\bUpdate\scopyright\b'),
    # found in breakpad
    ReFlags(
        r'LTEXT.*Copyright.*IDC_STATIC'),
    # found in a changelog
    ReFlags(
        r'copyright\ shortname'),
    # found in a translation
    ReFlags(
        r'copyright\ es\ ilegal'),
    ReFlags(
        r'copyright\ existente'),
    ReFlags(
        r'copyright\ existent'),
    # found in kdepimlibs
    ReFlags(
        r'copyright\ on\ that\ code'),
    # found in boringssl
    ReFlags(
        r'copyright\ on\ this\ code'),
    # found in telepathy-logger-qt
    ReFlags(
        r'for\ copyright\ in'),
    # changelog entry
    ReFlags(
        r'Fix\ copyright'),
    # found in marble
    ReFlags(
        r'concern\ over\ copyright'),
    ReFlags(
        r'copyright\ &copy;\ @copyright@'),
    # found in boringssl
    ReFlags(
        r'Copyright\ with\ his\ permission'),
    # found in dom_distiller_js
    ReFlags(
        r'optional\ string\ copyright\ ='),
    # found in ffmpeg
    ReFlags(
        r'copyright\ &\ original\ defaults'),
    ReFlags(
        r'--copyright'),
    ReFlags(
        r'skip\ copyright'),
    ReFlags(
        r'Copyright\ (?:Exists|chunk|length|offset|signature|it\.)'),
    # found in icu
    ReFlags(
        r'U_COPYRIGHT_STRING'),
    ReFlags(
        r'LATIN\ CAPITAL\ LETTER\ C'),
    ReFlags(
        r'copyright\ at\ the\ end'),
    ReFlags(
        r'copyright\ generation'),
    ReFlags(
        r'print\ our\ copyright'),
    ReFlags(
        r'copyright\ into\ the'),
    # found in spacezero
    ReFlags(
        r'AUTHOR\(S\)\ and\ COPYRIGHT\ sections'),
    # damn you (c)
    ReFlags(
        r'\bif\b.*\(\s?c\s?\)'),
    ReFlags(
        r'\bwhile\b.*\(\s?c\s?\)'),
    ReFlags(
        r'Similar\sto\s\(\s?c\s?\)'),
    ReFlags(
        r'\(\s?c\s?\)\sit\smakes\suse'),
    ReFlags(
        r'replaced\s\(C\)'),
    ReFlags(
        r'\(c\)\ have'),
    ReFlags(
        r'\(c\)\ the\ sum'),
    ReFlags(
        r'\(unsigned\ int\)'),
    ReFlags(
        r'\(\*?a\),\ ?\(b\),\ ?\(c\)'),
    # This is intended to match code using (c)
    # the .* here is a bit too greedy.
    ReFlags(
        r'\(\w\)\s*[^\ \t\n\r\f\va-zA-Z0-9_].*[^\s,;]\s*\(\w\)'),
    ReFlags(
        r'\(c\).*\(c2\)'),
    ReFlags(
        r'ISUPPER\ ?\(c\)'),
    ReFlags(
        r'\(c\)\ You\ must\ retain'),
    ReFlags(
        r'\(c\)\ ?[\*/&|<>\+-]?='),
    ReFlags(
        r'\(c\)\ ?\)'),
    ReFlags(
        r'sizeof\([^)]+\)'),
    ReFlags(
        r'\(c\)(?:->|\.)[^ ]'),
    ReFlags(
        r'=\s*\(c\)\s*\*.*;'),
    ReFlags(
        r'\(c\)\s*[*/+-]\s*\('),
    ReFlags(
        r'\(c\)\ and\ its\ data'),
    ReFlags(
        r'character\ \(c\)'),
    ReFlags(
        r'\(c\)\ differs\ for'),
    ReFlags(
        r'combining\ mark\ \(c\)'),
    ReFlags(
        r'\(c\)\ *processed'),
    ReFlags(
        r'\(c\)\ data\ (?:[^ ]+\ ){,5}has\ been\ received'),
    ReFlags(
        r'shown\sby\s\(c\)\ssupports'),
    ReFlags(
        r'AND\s\(c\)'),
    # more text
    ReFlags(
        r'following\scopyright\sand\slicense '),
    ReFlags(
        r'\bcopyright\ssign\b'),
    # found in qca
    ReFlags(
        r'"Country\ Code\ \(C\)"'),
    # found in cinnamon
    ReFlags(
        r'\bcopyright\ and\b'),
    # template used in po
    ReFlags(
        r'\bCopyright:?\s(?:\(\s*C\s*\)\s)?YEAR\b'),
    # found in cinnamon-settings-daemon
    ReFlags(
        r'free\sof\sknown\scopyright\srestrictions'),
    ReFlags(
        r'[-\s–—]%[ds][-\s–—]'),
    # template (marble)
    ReFlags(
        r'<\*\*authorname\*\*>'),
    # template (kcoreaddons)
    ReFlags(
        r'%{AUTHOR}'),
    # found in kcoreaddons
    ReFlags(
        r'i18n\("Copyright (?:[^ ]+ ){,4}'
        r'(?:Developer|Foo Foundation|Bar Foundation|Baz Foundation)',
    ),
    # tagged to be ignored
    ReFlags(
        r'krazy:exclude(?:all)?=[^\s]*copyright'),
    # found in kde-l10n
    ReFlags(
        r'copyright\ of\ every\ file\ belongs\ to\ its\s'),
    # Test case in ark
    ReFlags(
        r'UNRAR\s+[0-9.]+\ (\(iconv\)|beta\ [0-9])\ freeware\s+'
        r'Copyright\ \(c\)\ [0-9-]+\ Alexander\ Roshal'),
    # dll files interfaces
    ReFlags(
        r'\{[0-9A-F-]{32,}\}'),
)

COPYRIGHT_PRE_IGNORE_RES = tuple(
    re.compile(re_flag.re, re_flag.flags)
    for re_flag in COPYRIGHT_PRE_IGNORE_REGEXES
)

COPYRIGHT_INDICATOR_REGEXES = (
    # Multiline indicator
    ReFlags(
        r'(?:^|\s|\W)'
        r'('
        r'(?:[^ ]+\ ){,8}(?:is|are)\ '
        r'(?:'
        r'copyright\ by\ the[\s]+'
        r'|Copyright(?:\ \(C\))?[:\s]+'
        r'|the\ Copyright\ (?:<\w+>)?property\ of(?:[:\s]+|$)'
        r')'
        r')'
        r'(?=\S|$)'
    ),
    # The full word
    ReFlags(
        r'(?:^|\s|\W)(copyright(?:\s*\(c\))?(?:[:\s]+))(?=\S|$)'),
    # Legally-valid abbreviation
    ReFlags(
        r'(?:^|\s|\W)(copr\.(?:[:\s]+))(?=\S|$)'),
    # Unicode character COPYRIGHT SIGN
    ReFlags(
        r'(?:^|\s)(©(?:[:\s]+))(?=\S|$)'),
    # Unicode copyright sign encoded in iso8859
    ReFlags(
        r'(?:^|\s)(\xa9(?:[:\s]+))(?=\S|$)'),
    # HTML copyright sign
    ReFlags(
        r'(?:^|\s|\W)(&copy;(?:[:\s]+))(?=\S|$)'),
    # texi macro
    ReFlags(
        r'(?:^|\s|\W)(@copyright\{?\}?(?:[:\s]+))(?=\S|$)'),
    # html copyright sign
    ReFlags(
        r'(?:^|\s|\W)(&\#169;(?:[:\s]+))(?=\S|$)'),
    # Free text found in some debian/copyright
    ReFlags(
        r'(?:^|\s|\W)((?:Upstream\ Authors?\ ?(?:and|,)\ )?Copyright\ Holders(?:[:\s]+))(?=\S|$)'),
    # Legally-null representation of sign
    ReFlags(
        r'(?:^|\s|\W)((?:\(\s?c\s?\))[:\s]*)(?=\S|$)'),
)

COPYRIGHT_INDICATOR_RES = tuple(
    re.compile(re_flag.re, re_flag.flags)
    for re_flag in COPYRIGHT_INDICATOR_REGEXES
)

COPYRIGHT_INDICATOR_RE = re.compile(
    '|'.join(
        '{}'.format(re_flag.re) for re_flag in COPYRIGHT_INDICATOR_REGEXES
    ), re.I | re.M)

COPYRIGHT_PRE_INDICATOR_RE = re.compile(
    r'Copyright|copr\.|©|\xa9|&copy;|&\#169;|\(\s?c\s?\)',
    re.I | re.M)

COPYRIGHT_POST_IGNORE_REGEXES = (
    # Discussing copyright information
    ReFlags(
        r'\binformation\b', re.I),
    # Discussing the notice
    ReFlags(
        r'\bnotices?\b', re.I),
    ReFlags(
        r'\bstatements?\b', re.I),
    ReFlags(
        r'\bclaims?\b', re.I),
    ReFlags(
        r'\bstrings?\b', re.I),
    # GPL template
    ReFlags(
        r'\b\<?name\ of\ author\>?\b', re.I),
    # template
    ReFlags(
        r'\bYEAR\s+YOUR\s+NAME\b', re.I),
    ReFlags(
        r'\bYour\s+Name\s+\<your\.email@example\.com\>', re.I),
    ReFlags(
        r'{{copyright}}', re.I),
    # Part of a sentence
    # |and|or used in: and/or its subsidiary(-ies).
    # |in used in .in tld
    ReFlags(
        r'\b\ in\ \b', re.I),
    # |to used in "1990 to 1995"
    ReFlags(
        r'^\s*is\b', re.I),
    ReFlags(
        r'\bif\b', re.I),
    ReFlags(
        r'\bholders?\b', re.I),
    ReFlags(
        r'\bowners?\b', re.I),
    ReFlags(
        r'\bownership\b', re.I),
    # template
    ReFlags(
        r'\bIDC_STATIC\b', re.I),
    # (c) conditional
    ReFlags(
        r'(?:^|\s)&&(?:\s|$)', 0),
    ReFlags(
        r'(?:^|\s)\|\|(?:\s|$)', 0),
    # ugly one letter expressions
    ReFlags(
        r'(?:^|\s)\(\s?[abd-f]\s?\)(?:\s|$)', re.I),
    # GPL text
    ReFlags(
        r'^\s*law[.:]?\b', re.I),
    # if (c) {
    ReFlags(
        r'^\s*\{\s*$', 0),
    # Copyright: License
    ReFlags(
        r'^\s*L?GPL$\s*$', re.I),
    # template
    ReFlags(
        r'^\s*@\w+\{\w+\}\s*$', 0),
    # copyright of Qt has been transferred
    ReFlags(
        r'^\s*of\ [^\s]+\ has\b', re.I),
    # copyright applied to
    ReFlags(
        r'^\s*applied\ to\b', re.I),
    # QPL false positive
    ReFlags(
        r'^\s*Everyone\ is\ permitted\ to\ copy\b'),
)

COPYRIGHT_POST_IGNORE_RES = tuple(
    re.compile(re_flag.re, re_flag.flags)
    for re_flag in COPYRIGHT_POST_IGNORE_REGEXES
)

CRUFT_SUBS = (
    ReSub(reo.compile(
        r'''(?:(?:some|all)? rights reserved'''
        r'''|(?:some|all) rights)[\s,.;\*#'"]*''',
        reo.I), r''),
    ReSub(reo.compile(r'It can be distributed', reo.I), r''),
    ReSub(reo.compile(r'and contributors', reo.I), r''),
    # Drop the fsf address from the copyright
    ReSub(reo.compile(r'Franklin St(?:reet)?, Fifth Floor,?', reo.I), r''),
    ReSub(reo.compile(r'et al', reo.I), r''),
    ReSub(reo.compile(r'\band$', reo.I), r''),
    ReSub(reo.compile(r'\\$'), r''),
    ReSub(reo.compile(r'''[\s,.;\*#'"]*$'''), r''),
    ReSub(reo.compile(r'\(\sc\s\)', reo.I), r''),
    ReSub(reo.compile(r'</?\w{,2}/?>'), r''),  # Generic tag, <i> <br/>, etc
    ReSub(reo.compile(r'^(?:of|and)\s', reo.I), r''),
    # Unicode replacement char, present in badly converted files
    ReSub(reo.compile(r'�'), r''),
    ReSub(reo.compile(r'\s{2,}'), r' '),
    ReSub(reo.compile(r'^\s+'), r''),
    ReSub(reo.compile(r'\s+$'), r''),
    ReSub(reo.compile(r'\\@'), r'@'),
    ReSub(reo.compile(r'&ndash;'), r'-'),
)

EMAIL_SUBS = (
    ReSub(reo.compile(r'</?tt>'), r''),
    ReSub(reo.compile(r'%20'), r' '),
    # (HT|X)ML
    ReSub(reo.compile(r'(^\s?<!--|-->\s?$)'), r' '),
    ReSub(reo.compile(r'&lt;?'), r'<'),
    ReSub(reo.compile(r'&gt;?'), r'>'),
    ReSub(reo.compile(r'&#x40;'), r'@'),
    ReSub(reo.compile(r'&ldquo;?'), r'"'),
    ReSub(reo.compile(r'\(c\)$', reo.I), r''),
    ReSub(reo.compile(r'\\[nt]$', reo.I), r''),
    # Expensive fix for ") at the end of the string
    ReSub(reo.compile(r'((?P<paren>\()?(?(paren).*?|))(?(paren)|\)+)?$'), r'\1'),
    ReSub(reo.compile(r'\s+\(?(where|at|@)\)?\s+', reo.I), r'@'),
    ReSub(reo.compile(r'\(at\)', reo.I), r'@'),
    ReSub(reo.compile(r'\s+\(?do?[tm]\)?\s+', reo.I), r'.'),
    # Ugly fix for >mail@example.com<
    ReSub(reo.compile(r'(?:^|(?<=\s))\s*\>\s*(?=\w(?:\w|[.-])*@)'), r'<'),
    ReSub(reo.compile(r'\<\s*$'), r'>'),
    ReSub(reo.compile(r'(?:^|(?<=\s))\s*((?!\<)\w(?:\w|[.-])*@'
                      r'?:\w(?:\w|-)+(?:\.\w(?:\w|-)+)+(?<!>))\s*(?:(?=\s)|$)'),
          r'<\1>'),
    ReSub(reo.compile(r'\s\s+'), r' '),
    ReSub(reo.compile(r'^\s'), r''),
    ReSub(reo.compile(r'\s$'), r''),
)

NAME_CRUFT_SUBS = (
    ReSub(reo.compile(r'</item>', reo.I), r''),
    ReSub(reo.compile(r'^>', reo.I), r''),
    ReSub(reo.compile(r'<$', reo.I), r''),
    ReSub(reo.compile(r'\\[nt]$', reo.I), r''),
    ReSub(reo.compile(r'^\(\s*c\s*\)\s*', reo.I), r''),
    ReSub(reo.compile(r'^and$', reo.I), r''),
    ReSub(reo.compile(r'^and\s+(?:others|contributors)?\s*', reo.I), r''),
    ReSub(reo.compile(r'\bbut is\b.*$', reo.I), r''),
)

ARTISTIC_SUBS = (
    ReSub(
        re.compile(
            r'The "?Clarified "?Artistic License"? '
            r'Preamble ',
            re.IGNORECASE),
        'ClArtistic'),
    ReSub(
        re.compile(
            r'You may charge a reasonable copying fee for any '
            r'distribution of this Package. You may charge '
            r'any fee you choose for support of this Package. '
            r'You may not charge a fee for this Package itself. '
            r'However, you may distribute this Package in aggregate '
            r'with other \(possibly commercial\) programs as '
            r'part of a larger \(possibly commercial\) software '
            r'distribution provided that you do not advertise '
            r'this Package as a product of your own. You may embed '
            r'this Package[\' ]?s interpreter within an executable '
            r'of yours \(by linking\);? this shall be construed as '
            r'a mere form of aggregation, provided that the complete '
            r'Standard Version of the interpreter is so embedded.'
            r'.+'
            r'Aggregation of this Package with a commercial '
            r'distribution is always permitted provided that '
            r'the use of this Package is embedded; that is, '
            r'when no overt attempt is made to make this Package[\' ]?s '
            r'interfaces visible to the end user of the commercial '
            r'distribution. Such use shall not be construed as a '
            r'distribution of this Package.', re.IGNORECASE),
        'Artistic-1-Perl'),
    ReSub(
        re.compile(
            r'Aggregation of this Package with a commercial '
            r'distribution is always permitted provided that '
            r'the use of this Package is embedded; that is, '
            r'when no overt attempt is made to make this Package[\' ]?s '
            r'interfaces visible to the end user of the commercial '
            r'distribution. Such use shall not be construed as a '
            r'distribution of this Package.', re.IGNORECASE),
        'Artistic-1-cl8'),
    ReSub(
        re.compile(
            r'The "?Artistic License"? '
            r'Preamble '
            r'The intent of this document is to state the conditions under '
            r'which a Package may be copied, such that the Copyright Holder '
            r'maintains some semblance of artistic control over the '
            r'development of the package, while giving the users of the '
            r'package the right to use and distribute the Package in a '
            r'more[- ]or[- ]less customary fashion, plus the right to make '
            r'reasonable modifications.', re.IGNORECASE),
        'Artistic-1'),
    ReSub(
        re.compile(
            r'The Net Boolean Public License '
            r'Version ([\d.]+)',
            re.IGNORECASE),
        r'NBPL-\1'),
    ReSub(
        re.compile(
            r'The Open Group Test Suite License '
            r'Preamble',
            re.IGNORECASE),
        r'OGTSL'),
    ReSub(
        re.compile(
            r'The OpenLDAP Public License '
            r'Version ([\d.]+)',
            re.IGNORECASE),
        r'OLDAP-\1'),
)

BSD_SUBS = (
    ReSub(
        re.compile(
            r'4. If you include any Windows specific code \(or a derivative '
            r'thereof\) fromg? the apps directory \(application code\) you '
            r'must include an acknowledgement:? '
            r'"?This product includes software written by Tim Hudson '
            r'\(tjh@cryptsoft.com\)"?',
            re.IGNORECASE),
        'SSLeay'),
    ReSub(
        re.compile(
            r'Redistributions in any form must be accompanied by '
            r'information on how to obtain complete source code for the '
            r'software that uses (?:[^ ]+ ){,4}and any accompanying '
            r'software that uses the software that uses (?:[^ ]+ ){,4}'
            r'The source code must either be included in the '
            r'distribution or be available for no more than the cost of '
            r'distribution plus a nominal fee, and must be freely '
            r'redistributable under reasonable conditions\. For an '
            r'executable file, complete source code means the source code '
            r'for all modules it contains\. It does not include source code '
            r'for modules or files that typically accompany the major '
            r'components of the operating system on which the executable '
            r'file runs\. ',
            re.IGNORECASE),
        'BSD-like-TMate'),
    ReSub(
        re.compile(r'All advertising materials mentioning features or use of '
                   r'this software must display the following '
                   r'acknowledge?ment.*This product includes software '
                   r'developed by', re.IGNORECASE),
        'BSD-4-clause'),
    ReSub(
        re.compile(
            r'You acknowledge that this software is not designed'
            r'(?:, licensed)? '
            r'or intended for use in the design, construction, operation or '
            r'maintenance of any nuclear facility.', re.IGNORECASE),
        'BSD-3-clause-no-nuclear'),
    ReSub(
        re.compile(
            r'Redistributions of source code must retain the above '
            r'copyright notice, this list of conditions and the following '
            r'disclaimer.? '
            r'(?:\* )?'
            r'(?:The name(?:\(?s\)?)? .*? may not|Neither the '
            r'(?:names? .*?|authors?) ?n?or the names of(?: '
            r'(?:its|their|other|any))? contributors may) be used to '
            r'endorse or promote products derived from this software',
            re.IGNORECASE),
        'BSD-source-code'),
    ReSub(
        re.compile(
            r'Redistributions of any form whatsoever must retain the '
            r'following acknowledgment:? \'?This product includes software '
            r'developed by ',
            re.IGNORECASE),
        'BSD-3-clause-attribution'),
    ReSub(
        re.compile(
            r'The origin of this software must not be misrepresented;? '
            r'you must not claim that you wrote the original software. '
            r'If you use this software in a product, an acknowledgment '
            r'in the product documentation would be appreciated but is '
            r'not required.'
            r'.+'
            r'Altered source versions must be plainly marked as such, '
            r'and must not be misrepresented as being the original '
            r'software.'
            r'.+'
            r'The name of the author may not be used to endorse or '
            r'promote products derived from this software without '
            r'specific prior written permission.',
            re.IGNORECASE),
        'BSD-like-bzip2'),
    ReSub(
        re.compile(
            r'Intel Open Source License ', re.IGNORECASE),
        'Intel'),
    ReSub(
        re.compile(
            r'All redistributions must comply with the conditions imposed by '
            r'the University of California on certain embedded code, which '
            r'copyright Notice and conditions for redistribution are as '
            r'follows', re.IGNORECASE),
        'Sendmail'
    ),
    ReSub(
        re.compile(
            r'Redistributions in any form must be accompanied by '
            r'information on how to obtain complete source code for '
            r'the DB software and any accompanying software that uses the '
            r'DB software\. The source code must either be included in the '
            r'distribution or be available for no more than the cost of '
            r'distribution plus a nominal fee, and must be freely '
            r'redistributable under reasonable conditions\. For an '
            r'executable file, complete source code means the source code '
            r'for all modules it contains\. It does not include source code '
            r'for modules or files that typically accompany the major '
            r'components of the operating system on which the executable '
            r'file runs\.', re.IGNORECASE),
        'Sleepycat'
    ),
    ReSub(
        re.compile(r'(?:The name(?:\(?s\)?)? .*? may not|Neither the '
                   r'(?:names? .*?|authors?) n?or the names of(?: '
                   r'(?:its|their|other|any))? contributors may) be used to '
                   r'endorse or promote products derived from this software',
                   re.IGNORECASE),
        'BSD-3-clause'),
    ReSub(
        re.compile(
            r'Any additions, deletions, or changes to the original files '
            r'must be clearly indicated in accompanying documentation\. '
            r'including the reasons for the changes, and the names of '
            r'those who made the modifications\.',
            re.IGNORECASE),
        'BSD-like-Mup'),
    ReSub(
        re.compile(
            r'Redistributions in any form must be accompanied by information '
            r'on how to obtain complete source code for this software and '
            r'any accompanying software that uses this software\. The source '
            r'code must either be included in the distribution or be '
            r'available in a timely fashion for no more than the cost of '
            r'distribution plus a nominal fee, and must be freely '
            r'redistributable under reasonable and no more restrictive '
            r'conditions\. For an executable file, complete source code '
            r'means the source code for all modules it contains\. It does '
            r'not include source code for modules or files that typically '
            r'accompany the major components of the operating system on '
            r'which the executable file runs\. ',
            re.IGNORECASE),
        'BSD-like-TOSL'),
    ReSub(
        re.compile(
            r'The views and conclusions contained in the software and '
            r'documentation are those of the authors and should not '
            r'be interpreted as representing official policies, '
            r'either expressed or implied, of the FreeBSD Project.',
            re.IGNORECASE),
        'BSD-2-clause-FreeBSD'),
    ReSub(
        re.compile(
            r'This code is derived from software contributed to The '
            r'NetBSD Foundation by',
            re.IGNORECASE),
        'BSD-2-clause-NetBSD'),
    ReSub(
        re.compile(
            r'Nothing in this license shall be deemed to grant '
            r'any rights to trademarks, copyrights, patents, trade '
            r'secrets or any other intellectual property of A.M.P.A.S. '
            r'or any contributors, except as expressly stated herein, '
            r'and neither the name of A.M.P.A.S. nor of any other '
            r'contributors to this software, may be used to endorse '
            r'or promote products derived from this software without '
            r'specific prior written permission of A.M.P.A.S. or '
            r'contributor, as appropriate.', re.IGNORECASE),
        'BSD-2-clause-AMPAS'),
    ReSub(
        re.compile(
            r'This code is derived from software contributed to The '
            r'NetBSD Foundation by',
            re.IGNORECASE),
        'BSD-2-clause-NetBSD'),
    ReSub(
        re.compile(r'Redistributions of source code must retain the(?: above)? '
                   r'copyright notice', re.IGNORECASE),
        'BSD-2-clause'),
    ReSub(
        re.compile(
            r'BSD PROTECTION LICENSE '
            r'TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION, AND '
            r'MODIFICATION', re.IGNORECASE),
        'BSD-protection'),
    ReSub(
        re.compile(
            r'Permission to use, copy, modify, and/?or distribute this '
            r'software for any purpose with or without fee is hereby '
            r'granted. '
            r'THE SOFTWARE IS PROVIDED "?AS IS"? AND THE AUTHOR DISCLAIMS '
            r'ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL '
            r'IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO '
            r'EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, '
            r'INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER '
            r'RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN '
            r'ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, '
            r'ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE '
            r'OF THIS SOFTWARE.', re.IGNORECASE),
        '0BSD'),
    ReSub(
        re.compile(
            r'Redistributions of the Code in binary form must be '
            r'accompanied by this GPG-signed text in any documentation '
            r'and, each time the resulting executable program or a '
            r'program dependent thereon is launched, a prominent '
            r'display \(?e\.g\., splash screen or banner text\)? '
            r'of the Author[\' ]?s attribution information, which '
            r'includes:? '
            r'\(?a\)? Name \(?[" ]AUTHOR[" ]\)?, '
            r'\(?b\)? Professional identification \(?[" ]PROFESSIONAL '
            r'IDENTIFICATION[" ]\)?, and '
            r'\(?c\)? URL \(?[" ]URL[" ]\)?.', re.IGNORECASE),
        'AAL'),
    ReSub(
        re.compile(
            r'The Condor Team may publish revised and/or new versions '
            r'of this Condor Public License '
            r'\((?:``|[" ])?this License(?:''|[" ])?\) '
            r'from time to time\. Each '
            r'version will be given a distinguishing version number\. '
            r'Once Software has been published under a particular '
            r'version of this License, you may always continue to use '
            r'it under the terms of that version\. You may also '
            r'choose to use such Software under the terms of '
            r'any subsequent version of this License published '
            r'by the Condor Team\. No one other than the Condor '
            r'Team has the right to modify the terms of this License\.',
            re.IGNORECASE),
        'Condor-1.1+'),
    ReSub(
        re.compile(
            r'This product includes software developed by the EU DataGrid',
            re.IGNORECASE),
        'EUDatagrid'),
    ReSub(
        re.compile(
            r'Access and use of this software shall impose the following '
            r'obligations and understandings on the user\. The user is '
            r'granted the right, without any fee or cost, to use, copy, '
            r'modify, alter, enhance and distribute this software, and '
            r'any derivative works thereof, and its supporting '
            r'documentation for any purpose whatsoever, provided that '
            r'this entire notice appears in all copies of the software, '
            r'derivative works and supporting documentation\. Further, '
            r'(?:[^\s]+ ){,8}requests that the user credit '
            r'(?:[^\s]+ ){,8}in any publications that result from the '
            r'use of this software or in any product that includes '
            r'this software, although this is not an obligation\. The '
            r'names '
            r'(?:[^\s]+ ){,8}however, may not be used in any advertising '
            r'or publicity to endorse or promote any products or '
            r'commercial entity unless specific written permission is '
            r'obtained from '
            r'(?:[^\s]+ ){,8}The user also understands that '
            r'(?:[^\s]+ ){,8}is not obligated to provide the user with any '
            r'support, consulting, training or assistance of any kind with '
            r'regard to the use, operation and performance of this software '
            r'nor to provide the user with any updates, revisions, new '
            r'versions or "?bug fixes',
            re.IGNORECASE),
        'NetCDF'),
    ReSub(
        re.compile(
            r'You may make and give away verbatim copies of the source '
            r'form of the software without restriction, provided that you '
            r'duplicate all of the original copyright notices and '
            r'associated disclaimers\. '
            r'.{,4}You may modify your copy of the software in any way, '
            r'provided that you do at least ONE of the following:? ',
            re.IGNORECASE
        ),
        'Ruby'),
    ReSub(
        re.compile(
            r'Redistribution and use in source and binary forms -? ?with or '
            r'without modification -? ?are permitted for any purpose, '
            r'provided that redistributions in source form retain this '
            r'entire copyright notice and indicate the origin and nature of '
            r'any modifications\. '
            r'I\'d appreciate being given credit for this package in the '
            r'documentation of software which uses it, but that is not a '
            r'requirement\. ',
            re.IGNORECASE
        ),
        'Spencer-99'),
)

CC_BY_SUBS = (
    ReSub(
        re.compile(
            r'(?:https?[: ]/?/?creativecommons.org/licenses/by'
            r'|(?:Creative Commons|CC) (?:Legal Code )?Attribution'
            r')[- ]'
            r'(?:(?:ShareAlike|sa|NoDerivatives|NoDerivs|nd)[- ]){,2}'
            r'(?:NonCommercial|nc)',
            re.IGNORECASE),
        r'-NC'),
    ReSub(
        re.compile(
            r'(?:https?[: ]/?/?creativecommons.org/licenses/by'
            r'|(?:Creative Commons|CC) (?:Legal Code )?Attribution'
            r')[- ]'
            r'(?:(?:NonCommercial|nc|NoDerivatives|NoDerivs|nd)[- ]){,2}'
            r'(?:ShareAlike|sa)',
            re.IGNORECASE),
        r'-SA'),
    ReSub(
        re.compile(
            r'(?:https?[: ]/?/?creativecommons.org/licenses/by'
            r'|(?:Creative Commons|CC) (?:Legal Code )?Attribution'
            r')[- ]'
            r'(?:(?:NonCommercial|nc|ShareAlike|sa)[- ]){,2}'
            r'(?:NoDerivatives|NoDerivs|nd)',
            re.IGNORECASE),
        r'-ND'),
    ReSub(
        re.compile(
            r'(?:https?[: ]/?/?creativecommons.org/licenses/by'
            r'|(?:Creative Commons|CC) (?:Legal Code )?Attribution'
            r')[- ]'
            r'(?:'
            r'(?:NonCommercial|nc|ShareAlike|sa|NoDerivatives|NoDerivs|nd'
            r')[- ]?){,2}/?'
            r'(\d(?:\.\d[\d.]*)?)[^\d.]', re.IGNORECASE),
        r'-\1'),
)

CECILL_SUBS = (
    ReSub(
        re.compile(
            r'Une liste de questions fréquemment posées se '
            r'trouve sur le site web officiel de la famille '
            r'des licences CeCILL '
            r'\(https?[: ]/?/?www.cecill.info/index.fr.html\) '
            r'pour toute clarification qui serait nécessaire\.',
            re.IGNORECASE),
        'CeCILL-2.1'),
    ReSub(
        re.compile(
            r'Frequently asked questions can be found on the '
            r'official website of the CeCILL licenses family '
            r'\(https?[: ]/?/?www.cecill.info/index.en.html\)'
            r'for any necessary clarification\. ',
            re.IGNORECASE),
        'CeCILL-2.1'),
    ReSub(
        re.compile(
            r'un modèle de diffusion en logiciel libre',
            re.IGNORECASE),
        'CeCILL-2'),
    ReSub(
        re.compile(
            r'The exercising of these rights is conditional '
            r'upon certain obligations for users so as to preserve '
            r'this status for all subsequent redistributions\.',
            re.IGNORECASE),
        'CeCILL-2'),
    ReSub(
        re.compile(
            r'As a counterpart to the access to the source '
            r'code and rights to copy, modify and redistribute '
            r'granted by the license, users are provided only '
            r'with a limited warranty and the software\'s author, '
            r'the holder of the economic rights, and the successive '
            r'licensors only have limited liability\.',
            re.IGNORECASE),
        'CeCILL-1.1'),
    ReSub(
        re.compile(
            r'un modèle de diffusion (?:« ?)?open source ?»?',
            re.IGNORECASE),
        'CeCILL-1'),
    ReSub(
        re.compile(
            r'Nevertheless, access to the source code, and '
            r'the resulting rights to copy, modify and redistribute '
            r'only provide users with a limited warranty and the '
            r'software\'s author, the holder of the economic '
            r'rights, and the successive licensors only have '
            r'limited liability\. ',
            re.IGNORECASE),
        'CeCILL-1'),
)


CNRI_SUBS = (
    ReSub(
        re.compile(
            r'Licensee may not use CNRI trademarks or trade '
            r'name, including [^ ]+ or CNRI, in a trademark '
            r'sense to endorse or promote products or services '
            r'of Licensee, or any third party\. Licensee may '
            r'use the mark [^ ]+ in connection with Licensee\'s '
            r'derivative versions that are based on or incorporate '
            r'the Software, but only in the form ',
            re.IGNORECASE),
        'CNRI-Jython'),
    ReSub(
        re.compile(
            r'URL:? ?https?[: ]/?/?hdl\.handle\.net/'
            r'1895.22/1006',
            re.IGNORECASE),
        'CNRI-Jython'),
    ReSub(
        re.compile(
            r'Notwithstanding the foregoing, with regard to '
            r'derivative works based on '
            r'.+'
            r'that incorporate non[- ]separable material that '
            r'was previously distributed under the '
            r'GNU General Public License',
            re.IGNORECASE),
        'CNRI-Python-GPL-Compatible'),
    ReSub(
        re.compile(
            r'URL:? ?https?[: ]/?/?hdl\.handle\.net/'
            r'1895.22/1013',
            re.IGNORECASE),
        'CNRI-Python-GPL-Compatible'),
    ReSub(
        re.compile(
            r'URL:? ?https?[: ]/?/?hdl\.handle\.net/'
            r'1895.22/1011',
            re.IGNORECASE),
        'CNRI-Python'),
)

EFL_SUBS = (
    ReSub(
        re.compile(
            r'If the binary program depends on a modified version '
            r'of this package, you are encouraged to publicly '
            r'release the modified version of this package',
            re.IGNORECASE),
        'EFL-2'),
    ReSub(
        re.compile(
            r'if the binary program depends on a modified version '
            r'of this package, you must publicly release the '
            r'modified version of this package',
            re.IGNORECASE),
        'EFL-1'),
)

GNU_VERSION_SUBS = (
    ReSub(
        re.compile(
            r'version ([\d.]+) (?:of the License,? )?or '
            r'(?:\(? ?at your option ?\)? )?'
            r'version ([\d.]+),? or,? (?:(?:any )?later versions? '
            r'(?:accepted|approved) by the '
            r'membership of KDE ?e\.?V\.?|at the discretion of KDE ?e\.?V\.? '
            r'(?:[^ ]+ ){,20}any later version)', re.IGNORECASE),
        r'\1+\2+KDEeV'),
    ReSub(
        re.compile(
            r'General Public License version ([\d.]+) (?:and|or),? '
            r'(?:\(? ?at your option ?\)? )?version '
            r'([\d.]+) as published by the Free Software Foundation',
            re.IGNORECASE),
        r'\1+\2'),
    ReSub(
        re.compile(
            r'General Public License as published by the Free Software '
            r'Foundation[,;]? (?:either )?version ([\d.]+) '
            r'(?:of the License,? )?'
            r'(?:and|or),? (?:\(? ?at your option ?\)? )?(?:version )?'
            r'([\d.]+?)[.,;]? ',
            re.IGNORECASE),
        r'\1+\2'),
    ReSub(
        re.compile(
            r'version ([\d.]+) of the License,? or,? '
            r'(?:\(? ?at your option ?\)? )?'
            r'(?:any later version accepted by the '
            r'membership of KDE e.V.|at the discretion of KDE ?e\.?V\.? '
            r'(?:[^ ]+ ){,20}any later version)', re.IGNORECASE),
        r'\1+KDEeV'),
    ReSub(
        re.compile(r'either version ([^ ]+?)(?: of the License)?[;,] '
                   r'or[ ,(]+at your option[ ,)]+any later version',
                   re.IGNORECASE), r'\1+'),
    ReSub(
        re.compile(
            r'(?:modify it|used) under the terms of the GNU (?:Affero )?'
            r'(?:Lesser(?:/Library)? |Library(?:/Lesser)? )?General Public '
            r'Licen[cs]e (?:as published by the Free Software Foundation[;,]? )?'
            r'\(?(?:either )?version ?([^ ]+?)[;,)]* (?:as published by the Free '
            r'Software Foundation[;,]? )?(?:of the Licen[sc]e,? )?or '
            r'(?:later|(?:\(? ?at your option ?\)?,? )?any later version)',
            re.IGNORECASE),
        r'\1+'),
    ReSub(
        re.compile(r'licensed under (?:[^ ]+ ){,10}[AL]?GPL,? ?(?:version|v)'
                   r' ?([\d.]+) (?:(?:or )?(?:higher|later)|or[, (]+at your '
                   r'option[, )]+any later version)',
                   re.IGNORECASE),
        r'\1+'),
    ReSub(
        re.compile(
            r'General Public License,? Version ([\d.]+) or later',
            re.IGNORECASE),
        r'\1+'),
    ReSub(
        re.compile(
            r'under the terms of (?:the )?version ([^ ]+) or later of the '
            r'(?:GNU|[AL]?GPL)', re.IGNORECASE),
        r'\1+'),
    ReSub(
        re.compile(r'GNU (?:Affero )?(?:Lesser(?:/Library)? |Library )?'
                   r'General Public License (?:as )?published by the Free '
                   r'Software Foundation[;,] either version ([^ ]+?)[.,]? '
                   r'(?:of the License )?or '
                   r'(?:\(at your option\) )?any later version',
                   re.IGNORECASE),
        r'\1+'),
    ReSub(
        re.compile(r'licensed under (?:[^ ]+ ){,10}[AL]?GPL ?(?:version|v)'
                   r' ?([\d.]+?)[.,]? ',
                   re.IGNORECASE),
        r'\1'),
    ReSub(
        re.compile(r'(?:used|modify it) under the terms (?:and conditions )?'
                   r'of the GNU '
                   r'(?:Affero )?'
                   r'(?:Lesser(?:/Library)? |Library )?General Public '
                   r'License,? (?:as published by the Free Software '
                   r'Foundation;? )?(?:either )?'
                   r'Version ([\d.]+?)(?:[.,;]|as)? ', re.IGNORECASE), r'\1'),
    ReSub(
        re.compile(r'GNU (?:Affero )?(?:Lesser(?:/Library)? |Library )?'
                   r'General Public License, version ([^ ]+?)[ .,;]',
                   re.IGNORECASE), r'\1'),
    ReSub(
        re.compile(r'version ([\d.]+?)[.,]? (?:\(?only\)?.? )?'
                   r'(?:of the GNU (?:Affero )?'
                   r'(?:Lesser(?:/Library)? |Library )?'
                   r'General Public License )?(?:as )?published by the '
                   r'Free Software Foundation', re.IGNORECASE), r'\1'),
    ReSub(
        re.compile(r'GNU (?:Affero )?(?:Lesser(?:/Library)? |Library )?'
                   r'General Public License (?:as )?published by the Free '
                   r'Software Foundation[;,] version ([^ ]+?)[.,]? ',
                   re.IGNORECASE),
        r'\1'),
    ReSub(
        re.compile(
            r'(?:General Public License|GPL) (?:Vers.?ion )?([\d.]+?)[.,]? ',
            re.IGNORECASE),
        r'\1'),
)

GNU_EXCEPTION_SUBS = (
    ReSub(
        re.compile(
            r'As a special exception, if you create a document which uses '
            r'this font, and embed this font or unaltered portions of this '
            r'font into the document, this font does not by itself cause '
            r'the resulting document to be covered by the GNU General '
            r'Public License. This exception does not however invalidate '
            r'any other reasons why the document might be covered by the '
            r'GNU General Public License. If you modify this font, you may '
            r'extend this exception to your version of the font, but you '
            r'are not obligated to do so. If you do not wish to do so, '
            r'delete this exception statement from your version.',
            re.IGNORECASE),
        r'Font'),
    ReSub(
        re.compile(
            r'As a special exception, the copyright holders give permission '
            r'to link the code of portions of this program with the OpenSSL '
            r'library', re.IGNORECASE),
        r'OpenSSL'),
    ReSub(
        re.compile(
            r'(?:'
            r'As a special exception(?:, the respective Autoconf Macro[\' ]?s '
            r'copyright owner gives unlimited permission to copy, distribute '
            r'and modify the configure scripts that are the output of '
            r'Autoconf when processing the Macro\.'
            r'| to the GNU General Public License, if you distribute this '
            r'file as part of a program that contains a configuration '
            r'script generated by Autoconf, you may include it under the '
            r'same distribution terms that you use for the rest of that '
            r'program\.'
            r'|, the Free Software Foundation gives unlimited permission to '
            r'copy, distribute and modify the configure scripts that are '
            r'the output of Autoconf\.)'
            r'|'
            r'This Exception is an additional permission under section 7 of '
            r'the GNU General Public License, version 3 '
            r'\([" ]?GPLv3[" ]?\)\. It '
            r'applies to a given file that bears a notice placed by the '
            r'copyright holder of the file stating that the file is '
            r'governed by GPLv3 along with this Exception\. '
            r'The purpose of this Exception is to allow distribution of '
            r'Autoconf[\' ]?s typical output under terms of the '
            r'recipient[\' ]?s '
            r'choice \(including proprietary\)\.'
            r')', re.IGNORECASE),
        r'AutoConf'),
    ReSub(
        re.compile(
            r'As a special exception, you may create a larger work that '
            r'contains part or all of the Bison parser skeleton and '
            r'distribute that work under terms of your choice', re.IGNORECASE),
        r'Bison'),
    ReSub(
        re.compile(
            r'As a special exception, the copyright holders of this library '
            r'give you permission to link this library with independent '
            r'modules to produce an executable, regardless of the license '
            r'terms of these independent modules, and to copy and distribute '
            r'the resulting executable under terms of your choice, provided '
            r'that you also meet, for each linked independent module, the '
            r'terms and conditions of the license of that module\.',
            re.IGNORECASE),
        r'ClassPath'),
    ReSub(
        re.compile(
            r'(?:'
            r'In addition to the permissions in the GNU General Public '
            r'License, the Free Software Foundation gives you unlimited '
            r'permission to link the compiled version of this file into '
            r'combinations with other programs, and to distribute those '
            r'combinations without any restriction coming from the use of '
            r'this file\.'
            r'|'
            r'This GCC Runtime Library Exception '
            r'\([" ]?Exception[" ]?\) is an additional permission under '
            r'section 7 of the GNU General Public License, version 3 '
            r'\([" ]?GPLv3[" ]?\)\. It applies to a given file \(the '
            r'"?Runtime Library[" ]?\) that bears a notice placed by the '
            r'copyright holder of the file stating that the file is '
            r'governed by GPLv3 along with this Exception\.'
            r')',
            re.IGNORECASE),
        r'GCC'),
    ReSub(
        re.compile(
            r'As a special exception to the (?:[^ ]+ ){,5}if you '
            r'distribute this file as part of a program or library that '
            r'is built using GNU Libtool, you may include this file under '
            r'the same distribution terms that you use for the rest of that '
            r'program', re.IGNORECASE),
        r'LibTool'),
    ReSub(
        re.compile(
            r'In addition, as a special exception, (?:[^\s]+ ){,8}'
            r'gives You the additional right to link the code of '
            r'this Program with code not covered under the GNU '
            r'General Public License \(?[" ]?Non[- ]?GPL Code[" ]?\)? and '
            r'to distribute linked combinations including the '
            r'two, subject to the limitations in this paragraph. '
            r'Non-GPL Code permitted under this exception must only '
            r'link to the code of this Program through those well '
            r'defined interfaces identified in the file named EXCEPTION '
            r'found in the source code files \(?the "?Approved '
            r'Interfaces[" ]?[\) ]?\. The files of Non[- ]?GPL Code may '
            r'instantiate templates or use macros or inline functions '
            r'from the Approved Interfaces without causing the resulting '
            r'work to be covered by the GNU General Public License. Only '
            r'(?:[^\s]+ ){,8}may make changes or additions to the list of '
            r'Approved Interfaces. You must obey the GNU General Public '
            r'License in all respects for all of the Program code and '
            r'other code used in conjunction with the Program except the '
            r'Non-GPL Code covered by this exception. If you modify this '
            r'file, you may extend this exception to your version of the '
            r'file, but you are not obligated to do so. If you do not '
            r'wish to provide this exception without modification, you '
            r'must delete this exception statement from your version and '
            r'license this file solely under the GPL without exception.',
            re.IGNORECASE),
        r'389'),
    ReSub(
        re.compile(
            r'This copyright does NOT cover user programs that '
            r'run in CLISP and third-party packages not part '
            r'of CLISP, if a',
            re.IGNORECASE),
        r'CLISP'),
    ReSub(
        re.compile(
            r'A FOSS application developer \([“" ]?you[”" ]? or '
            r'[“" ]?your[”" ]?\) may distribute a Derivative '
            r'Work provided that you and the Derivative Work '
            r'meet all of the following conditions',
            re.IGNORECASE),
        r'DigiRule-FOSS'),
    ReSub(
        re.compile(
            r'As a special exception, if other files instantiate '
            r'templates or use macros or inline functions from '
            r'this file, or you compile this file and link '
            r'it with other works to produce a work based on '
            r'this file, this file does not by itself cause '
            r'the resulting work to be covered by the GNU General '
            r'Public License\. However the source code for '
            r'this file must still be made available in accordance '
            r'with section \(3\) of the GNU General Public License\.',
            re.IGNORECASE),
        r'eCos-2'),
    ReSub(
        re.compile(
            r'The FLTK library and included programs are provided '
            r'under the terms of the GNU Library General Public '
            r'License \(LGPL\) with the following exceptions',
            re.IGNORECASE),
        r'FLTK'),
    ReSub(
        re.compile(
            r'As a special exception, the copyright holder of '
            r'FreeRTOS gives you permission to link FreeRTOS '
            r'with independent modules that communicate with '
            r'FreeRTOS solely through the FreeRTOS API interface, '
            r'regardless of the license terms of these independent '
            r'modules, and to copy and distribute the resulting '
            r'combined work under terms of your choice, provided '
            r'that '
            r'Every copy of the combined work is accompanied by '
            r'a written statement that details to the recipient '
            r'the version of FreeRTOS used and an offer by yourself '
            r'to provide the FreeRTOS source code \(including any '
            r'modifications you may have made\) should the '
            r'recipient request it.',
            re.IGNORECASE),
        r'FreeRTOS'),
    ReSub(
        re.compile(
            r'As a special exception, if you link this library '
            r'with other files to produce an executable, this '
            r'library does not by itself cause the resulting '
            r'executable to be covered by the GNU General Public '
            r'License.',
            re.IGNORECASE),
        r'GNU-JavaMail'),
    ReSub(
        re.compile(
            r'In addition, as a special exception, .+'
            r'gives permission to link the code of this program '
            r'with the proprietary Java implementation provided '
            r'by Sun \(or other vendors as well\), and distribute '
            r'linked combinations including the two.',
            re.IGNORECASE),
        r'i2p-Java'),
    ReSub(
        re.compile(
            r'Special exception for LZMA compression module',
            re.IGNORECASE),
        r'LZMA'),
    ReSub(
        re.compile(
            r'if other files instantiate templates '
            r'or use macros or inline functions from this file, '
            r'or you compile this file and link it with other '
            r'files to produce an executable, this file does '
            r'not by itself cause the resulting executable '
            r'to be covered by the GNU General Public License.',
            re.IGNORECASE),
        r'MIF'),
    ReSub(
        re.compile(
            r'Open CASCADE Exception \(version ([\d.]+)\) '
            r'to GNU ',
            re.IGNORECASE),
        r'OCCT-\1'),
    ReSub(
        re.compile(
            r'Special exception for linking OpenVPN with OpenSSL',
            re.IGNORECASE),
        r'openvpn-openssl'),
    ReSub(
        re.compile(
            r'Qt LGPL Exception version ([\d.]+) '
            r'As an additional permission to the GNU ',
            re.IGNORECASE),
        r'Qt\1'),
    ReSub(
        re.compile(
            r'U-Boot License Exception:? '
            r'Even though ',
            re.IGNORECASE),
        r'u-boot'),
    ReSub(
        re.compile(
            r'EXCEPTION NOTICE '
            r'.{,4}As a special exception, the copyright holders '
            r'of this library give permission for additional '
            r'uses of the text contained in this release of '
            r'the library as licenced under the wxWindows '
            r'Library Licen[cs]e, applying either version 3.1 '
            r'of the Licen[cs]e, or \(at your option\) any later '
            r'version of the Licen[cs]e as published by the '
            r'copyright holders of version 3.1 of the Licen[cs]e '
            r'document\.',
            re.IGNORECASE),
        r'WxWindows-3.1+'),
)

GFDL_SUBS = (
    ReSub(
        re.compile(
            r'GNU Free Documentation License[,.; ]*(?:(?:Version|v)? [^ ]* )?'
            r'(?:or any later version published by the Free Software '
            r'Foundation; )?'
            r'with no Invariant Sections, no Front-Cover Texts, and no '
            r'Back-Cover Texts', re.IGNORECASE),
        r'-NIV'),
    ReSub(
        re.compile(
            r'GNU Free Documentation License[,.; ]*(?:Version|v)? ?'
            r'(\d+(?:\.[\d]+)*)[-,.; ]+?', re.IGNORECASE),
        r'-\1'),
)

LPPL_SUBS = (
    ReSub(
        re.compile(
            r'LaTeX Project Public License'
            r'(?: Distributed from CTAN archives in directory '
            r'macros/latex/base/lppl\.txt)?[,;]? (?:either )?(?:version|v) '
            r'?([\d.]+)[; ]', re.IGNORECASE),
        r'-\1'),
    ReSub(
        re.compile(
            r'LaTeX Project Public License,? (?:[^ ]+ ){,16}or '
            r'(?:\(?at your option\)? )?any later version', re.IGNORECASE),
        r'+'),
)

MIT_SUBS = (
    ReSub(
        re.compile(
            r'The above copyright notice and this permission notice shall '
            r'be included in all copies of the Software, its documentation '
            r'and marketing (?:&? )?publicity materials, and acknowledgment '
            r'shall be given in the documentation, materials and software '
            r'packages that this Software was used.',
            re.IGNORECASE),
        'MIT-advertising'),
    ReSub(
        re.compile(
            r'Permission to use, copy, modify, and distribute this software '
            r'and its documentation for any purpose and without fee is '
            r'hereby granted, provided that the above copyright notice '
            r'appear in all copies and that both that the copyright notice '
            r'and this permission notice and warranty disclaimer appear in '
            r'supporting documentation, and that the name of the author not '
            r'be used in advertising or publicity pertaining to distribution '
            r'of the software without specific, written prior permission.',
            re.IGNORECASE),
        'Expat-like-Highscore'),
    ReSub(
        re.compile(
            r'Except as contained in this notice, the name of a copyright '
            r'holder shall not be used in advertising or otherwise to '
            r'promote the sale, use or other dealings in this Software '
            r'without prior written authorization of the copyright holder.',
            re.IGNORECASE),
        'MIT-like-icu'),
    ReSub(
        re.compile(
            r'Except as contained in this notice, the name of a copyright '
            r'holder shall not be used in advertising or otherwise to '
            r'promote the sale, use or other dealings in this Software '
            r'without prior written authorization of the copyright holder.',
            re.IGNORECASE),
        'MIT-like-icu'),
    ReSub(
        re.compile(
            r'The above copyright notice and this permission notice shall '
            r'be included in all copies of the Software and its '
            r'Copyright notices. In addition publicly documented '
            r'acknowledgment must be given that this software has '
            r'been used if no source code of this software is '
            r'made available publicly. This includes acknowledgments '
            r'in either Copyright notices, Manuals, Publicity and '
            r'Marketing documents or any documentation provided '
            r'with any product containing this software. This '
            r'License does not apply to any software that links '
            r'to the libraries provided by this software \(?statically '
            r'or dynamically[\) ]?, but only to the software provided.',
            re.IGNORECASE),
        'MIT-enna'),
    ReSub(
        re.compile(
            r'The above copyright notice and this permission '
            r'notice shall be included in all copies of the '
            r'Software and its documentation and acknowledgment '
            r'shall be given in the documentation and software '
            r'packages that this Software was used.', re.IGNORECASE),
        'MIT-feh'),
    ReSub(
        re.compile(
            r'Distributions of all or part of the Software intended to be '
            r'used by the recipients as they would use the unmodified '
            r'Software, containing modifications that substantially alter, '
            r'remove, or disable functionality of the Software, '
            r'outside of the documented configuration mechanisms '
            r'provided by the Software, shall be modified such '
            r'that the Original Author[\' ]?s bug reporting '
            r'email addresses and urls are either replaced '
            r'with the contact information of the parties responsible '
            r'for the changes, or removed entirely.',
            re.IGNORECASE),
        'MITNFA'),
    ReSub(
        re.compile(
            r'The above copyright notice and this permission notice shall '
            r'be included in all copies of the Software and its Copyright '
            r'notices. In addition publicly documented acknowledgment must '
            r'be given that this software has been used if no source code '
            r'of this software is made available publicly. Making the source '
            r'available publicly means including the source for this '
            r'software with the distribution, or a method to get this '
            r'software via some reasonable mechanism \(electronic transfer '
            r'via a network or media\) as well as making an offer to supply '
            r'the source on request. This Copyright notice serves as an '
            r'offer to supply the source on (?:on )?request as well. Instead '
            r'of this, supplying acknowledgments of use of this software in '
            r'either Copyright notices, Manuals, Publicity and Marketing '
            r'documents or any documentation provided with any product '
            r'containing this software. This License does not apply to any '
            r'software that links to the libraries provided by this '
            r'software \(statically or dynamically\), but only to the '
            r'software provided.',
            re.IGNORECASE),
        'MIT-Imlib2'),
    ReSub(
        re.compile(
            r'The above copyright notice including the dates of first '
            r'publication and either this permission notice or a reference '
            r'to https?[: ]/?/?oss.sgi.com/projects/FreeB/ shall be '
            r'included in all copies or substantial portions of the '
            r'Software\.',
            re.IGNORECASE),
        'SGI-B-2'),
    ReSub(
        re.compile(
            r'The above copyright notice including the dates of first '
            r'publication and either this permission notice or a reference '
            r'to https?[: ]/?/?oss.sgi.com/projects/FreeB/ shall be '
            r'included in all copies or substantial portions of the '
            r'Software\.',
            re.IGNORECASE),
        'SGI-B-2'),
    ReSub(
        re.compile(
            r'Permission to use, copy, modify, and distribute this software '
            r'and its documentation for any purpose and without fee is '
            r'hereby granted, provided that the above copyright notice '
            r'appear in all copies and that both the copyright notice and '
            r'this permission notice and warranty disclaimer appear in '
            r'supporting documentation, and that the name of '
            r'(?:[^ ]+ ){,16}not be used in advertising or publicity '
            r'pertaining to distribution of the software without specific, '
            r'written prior permission\. '
            r'(?:[^ ]+ ){,4}disclaims all warranties with regard to this '
            r'software, including all implied warranties of merchantability '
            r'and fitness. In no event shall (?:[^ ]+ ){,4}be liable for '
            r'any special, indirect or consequential damages or any damages '
            r'whatsoever resulting from loss of use, data or profits, '
            r'whether in an action of contract, negligence or other '
            r'tortious action, arising out of or in connection with the use '
            r'or performance of this software\.',
            re.IGNORECASE),
        'MIT-like-SMLNJ'),
    ReSub(
        re.compile(
            r'The end-user documentation included with the redistribution, '
            r'if any, must include the following acknowledgment:? "?This '
            r'product includes software developed by The XFree86 Project, '
            r'Inc \(https?[: ]/?/?www.xfree86.org[/ ]?\) and its '
            r'contributors[" ]?, in the same place and form as other '
            r'third-party acknowledgments\. Alternately, this acknowledgment '
            r'may appear in the software itself, in the same form and '
            r'location as other such third-party acknowledgments\. ',
            re.IGNORECASE),
        'XFree86-1.1'),
)

ZPL_SUBS = (
    ReSub(
        re.compile(
            r'(?:the )?Zope Public License(?: \(?ZPL\)?)?'
            r',? (?:Version|v)?[ ]?([\d.]+?)(?:[.0]*)[^\d.]',
            re.IGNORECASE),
        r'-\1'),
)

LICENSES_RES = (
    # public-domain
    ReLicense(
        r'(:?This [^ ]+ is in|is (?:hereby |released )?'
        r'(?:in(?:to)|to|for)) '
        r'the public domain',
        'public-domain'),
    ReLicense(
        r'This work is free of known copyright restrictions',
        'public-domain'),
    # Splitted to avoid having alternatives at the start of the regexp
    ReLicense(
        r'http:[/ ]{,2}creativecommons.org/publicdomain/mark/',
        'public-domain'),
    ReLicense(
        r'https:[/ ]{,2}creativecommons.org/publicdomain/mark/',
        'public-domain'),
    ReLicense(
        r'Public Domain, originally written by',
        'public-domain'),
    ReLicense(
        r'This [^ ]+ is put into the public domain, I claim '
        r'no copyright on that code',
        'public-domain'),
    ReLicense(
        r' was written by(?: [^ ]+){,4}, and is placed in the '
        r'public domain\. The author hereby disclaims copyright to '
        r'this source code\.',
        'public-domain'),
    ReLicense(
        r'These files[] ]+are hereby placed in the public domain without '
        r'restrictions',
        'public-domain'),
    ReLicense(
        r'I hereby disclaim the copyright on this code and place it in '
        r'the public domain.',
        'public-domain'),
    # Apache
    ReLicense(
        r'under the Apache License, Version ([\d.]+)',
        'Apache', _name_version),
    ReLicense(
        r'Licensed under the Apache License v([\d.]+) '
        r'https?[ :]/?/?www\.apache\.org/licenses/LICENSE-(?:[\d.]+)',
        'Apache',
        _name_version),
    ReLicense(
        r'Apache License Version ([\d.]+), \w+ \d+ '
        r'https?[ :]/?/?www\.apache\.org/licenses/? '
        r'TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION',
        'Apache', _name_version),
    ReLicense(
        r'Redistribution and use in source and binary forms, '
        r'with or without modification, are permitted provided '
        r'that the following conditions are met:? '
        r'(?:[^\s]+ ){,2}'
        r'Redistributions of source code must retain the '
        r'above copyright notice, this list of conditions '
        r'and the following disclaimer. '
        r'(?:[^\s]+ ){,2}'
        r'Redistributions in binary form must reproduce the '
        r'above copyright notice, this list of conditions '
        r'and the following disclaimer in the documentation '
        r'and[/ ]?or other materials provided with the '
        r'distribution. '
        r'(?:[^\s]+ ){,2}'
        r'All advertising materials mentioning features '
        r'or use of this software must display the following '
        r'acknowledgment:? "?This product includes software '
        r'developed by (?:[^\s]+ ){,32}'
        r'(?:[^\s]+ ){,2}'
        r'(?:[^\s]+ ){,32}must not be used to endorse or '
        r'promote products derived from this software without '
        r'prior written permission. For written permission, '
        r'please contact (?:[^\s]+ ){,8}'
        r'(?:[^\s]+ ){,2}'
        r'Products derived from this software may not be '
        r'called (?:[^\s]+ ){,8}nor may (?:[^\s]+ ){,8}'
        r'appear in their name, without prior written permission '
        r'of (?:[^\s]+ ){,8}'
        r'(?:[^\s]+ ){,2}'
        r'Redistributions of any form whatsoever must retain '
        r'the following acknowledgment:? '
        r'"?This product includes software developed by ',
        'Apache',
        lambda *a: 'Apache-1'
    ),
    ReLicense(
        r'Redistribution and use in source and binary forms, '
        r'with or without modification, are permitted provided '
        r'that the following conditions are met:? '
        r'(?:[^\s]+ ){,2}'
        r'Redistributions of source code must retain the '
        r'above copyright notice, this list of conditions '
        r'and the following disclaimer. '
        r'(?:[^\s]+ ){,2}'
        r'Redistributions in binary form must reproduce the '
        r'above copyright notice, this list of conditions '
        r'and the following disclaimer in the documentation '
        r'and[/ ]?or other materials provided with the '
        r'distribution. '
        r'(?:[^\s]+ ){,2}'
        r'The end[- ]?user documentation included with the '
        r'redistribution, if any, must include the following '
        r'acknowledgment:? '
        r'"?This product includes software developed by .+."? '
        r'Alternately, this acknowledgment may appear in '
        r'the software itself, if and wherever such '
        r'third[- ]?party acknowledgments normally appear. '
        r'(?:[^\s]+ ){,2}'
        r'The .+ must not be used to endorse or promote '
        r'products derived from this software without prior '
        r'written permission. For written permission, please '
        r'contact .+ '
        r'(?:[^\s]+ ){,2}'
        r'Products derived from this software may not be '
        r'called .+ nor may .+ appear in their name, without '
        r'prior written permission of',
        'Apache',
        lambda *a: 'Apache-1.1'
    ),
    # Artistic
    ReLicense(
        r'Released under the terms of the Artistic License '
        r'(?:v|version )?([\d.]+)',
        'Artistic', _name_version),
    ReLicense(
        r'is distributed under the Artistic License '
        r'(?:v|version )?([\d.]+)',
        'Artistic', _name_version),
    ReLicense(
        r'Preamble '
        r'The intent of this document is to state the conditions under '
        r'which a Package may be copied, such that the Copyright Holder '
        r'maintains some semblance of artistic control over the '
        r'development of the package, while giving the users of the '
        r'package the right to use and distribute the Package in a '
        r'more[- ]or[- ]less customary fashion, plus the right to make '
        r'reasonable modifications.',
        'Artistic'),
    ReLicense(
        r'"?Artistic License"? 2\.0'
        r'.+'
        r'Everyone is permitted to copy and distribute '
        r'verbatim copies of this license document, but '
        r'changing it is not allowed.',
        'Artistic',
        lambda *a: 'Artistic-2'),
    ReLicense(
        r'you can (?:re)?distribute (?:it and/or modify '
        r'it )?under the terms of.* the Artistic License',
        'Artistic'),
    ReLicense(
        r'you may (?:re)?distribute (?:it and/or modify '
        r'it )?under the terms of.* the Artistic License',
        'Artistic'),
    ReLicense(
        r'is free software under the Artistic License',
        'Artistic'),
    # BSD-2-clause BSD-3-clause BSD-4-clause
    ReLicense(
        r'licensed under the (?:[^ ]+ ){,3}'
        r'\(?https?[ :]/?/?(?:www\.)?opensource\.org/licenses/BSD-([23])-Clause\)?',
        'BSD',
        lambda t, m, l:
            'BSD-{}-clause'.format(m.group(1))
    ),
    ReLicense(
        r'THIS FREE SOFTWARE IS PROVIDED .*"?AS IS(?:\'\'|")? '
        r'AND ANY '
        r'EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT '
        r'LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY',
        'BSD'),
    ReLicense(
        r'THIS SOFTWARE IS PROVIDED .*"?AS IS(?:\'\'|")? '
        r'AND ANY '
        r'EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT '
        r'LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY',
        'BSD'),
    ReLicense(
        r'''THIS SOFTWARE IS PROVIDED (?:``|")?AS IS(?:''|")? AND '''
        r'WITHOUT ANY EXPRESS OR '
        r'IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE '
        r'IMPLIED WARRANTIES OF MERCHANTABILITY',
        'BSD'),
    ReLicense(
        r'licensed under (?:[^ ]+ ){,10}the BSD license',
        'BSD'),
    ReLicense(
        r'Redistribution and use in source and binary forms, with '
        r'or without modification, are permitted provided that '
        r'the following conditions are met.{,32}'
        r'Redistributions of source code must retain the above '
        r'copyright notice, this list of conditions and the '
        r'following disclaimer.{,32}'
        r'Redistributions in binary form must reproduce the above '
        r'copyright notice, this list of conditions and the '
        r'following disclaimer in the documentation and/or other '
        r'materials provided with the distribution.{,32}'
        r'The name "?Carnegie Mellon University"? must not be used '
        r'to endorse or promote products derived from this '
        r'software without prior written permission. For '
        r'permission or any other legal details, please contact '
        r'Office of Technology Transfer Carnegie Mellon '
        r'University.{,256}'
        r'Redistributions of any form whatsoever must retain the '
        r'following acknowledgment:? "?This product includes '
        r'software developed by Computing Services at Carnegie '
        r'Mellon University \(https?[: ]/?/?www.cmu.edu/?computing/?\)."?',
        'BSD',
        lambda *a: 'BSD_like_CMU-4-clause'),
    ReLicense(
        r'Redistribution and use is allowed according to the terms of '
        r'the 2-clause BSD license.',
        'BSD',
        lambda *a: 'BSD-2-clause'),
    ReLicense(
        r'Redistribution and use is allowed according to the terms of the '
        r'(?:new )?BSD license.? ([^ ]+ ){,10}COPYING-CMAKE-SCRIPTS',
        'BSD',
        lambda *a: 'BSD-3-clause'),
    ReLicense(
        r'Redistribution and use is allowed according to the terms of the '
        r'BSD license.?',
        'BSD'),
    ReLicense(
        r'Distributed under the OSI-approved BSD License \(the '
        r'"?License[" ]?\)',
        'BSD'),
    # TODO: learn about references to named files
    ReLicense(
        r'Use of this source code is governed by a BSD-style license '
        r'that can be found in the [^ ]+ file\.',
        'BSD'),
    # ISC
    ReLicense(
        r'Permission to use, copy, modify, and(?:\/or)? distribute '
        r'this (?:[^ ]+ ){,4}for any purpose with or without fee is '
        r'hereby granted, provided.*copyright notice.*permission '
        r'notice.*all copies'
        r'.+'
        r'Except as contained in this notice, the '
        r'name of a copyright holder shall not be '
        r'used in advertising or otherwise to promote '
        r'the sale, use or other dealings in this '
        r'Software without prior written authorization '
        r'of the copyright holder',
        'ISC',
        lambda *a: 'curl'),
    ReLicense(
        r'Permission to use, copy, modify, and(?:\/or)? distribute '
        r'this (?:[^ ]+ ){,4}for any purpose with or without fee is '
        r'hereby granted, provided.*copyright notice.*permission '
        r'notice.*all copies',
        'ISC'),
    # CC-BY, CC-BY-SA, CC-BY-ND, CC-BY-NC, CC-BY-NC-SA, CC-BY-NC-ND
    ReLicense(
        r'http:[/ ]{,2}creativecommons.org/licenses/by',
        'CC-BY'),
    ReLicense(
        r'https:[/ ]{,2}creativecommons.org/licenses/by',
        'CC-BY'),
    ReLicense(
        r'is licensed under .*Creative Commons Attribution'
        r'.*License',
        'CC-BY'),
    ReLicense(
        r'THE WORK \(?AS DEFINED BELOW\)? IS PROVIDED UNDER THE TERMS '
        r'OF THIS CREATIVE COMMONS PUBLIC LICENSE \(?[" ]?CCPL[" ]? OR '
        r'[" ]?LICENSE[" ]?\)?\. THE WORK IS PROTECTED BY COPYRIGHT '
        r'AND/OR OTHER '
        r'APPLICABLE LAW\. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED '
        r'UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED\.',
        'CC-BY'),
    ReLicense(
        r'License:? (?:Creative Commons|CC) Attribution',
        'CC-BY'),
    # CC0
    ReLicense(
        r'http:[/ ]{,2}creativecommons.org/publicdomain/zero',
        'CC0'),
    ReLicense(
        r'https:[/ ]{,2}creativecommons.org/publicdomain/zero',
        'CC0'),
    ReLicense(
        r'To the extent possible under law, .*the person who '
        r'associated CC0 .*with this work has waived all copyright '
        r'and related or neighboring rights to this work',
        'CC0'),
    ReLicense(
        r'license:? CC0-1\.0 ',
        'CC0'),
    ReLicense(
        r'Creative Commons CC0 1\.0 ',
        'CC0'),
    # CDDL
    ReLicense(
        r'terms of the Common Development and Distribution License'
        r'(, Version ([^( ]+))? \(the[" ]*License[" ]?\)',
        'CDDL',
        lambda t, m, l:
            'CDDL{}'.format('-{}'.format(m.group(2)) if m.groups()[1] else '')
    ),
    ReLicense(
        r'COMMON DEVELOPMENT AND DISTRIBUTION LICENSE '
        r'\(CDDL\) '
        r'Version ([\d.]+)',
        'CDDL', _name_version),
    # CPL
    ReLicense(
        r'under the terms of '
        r'.*(the|this) Common Public License',
        'CPL',
        lambda *a: 'CPL-1'),
    ReLicense(
        r'(?:licensed|released) under the '
        r'.*(the|this) Common Public License',
        'CPL',
        lambda *a: 'CPL-1'),
    # EFL
    ReLicense(
        r'Permission is hereby granted to use, copy, modify and(?:/or) '
        r'distribute this [^ ]+ provided that:?'
        r'[-,.+* ]*copyright notices are retained unchanged'
        r'[-,.+* ]*any distribution of this [^ ]+ whether modified or '
        r'not, includes this (?:file|license text)[-,.+* \d]*'
        r'Permission is hereby also granted to distribute binary programs '
        r'which depend on this [^ ]+(?:provided that:)?'
        r'[-,.+* ]*if the binary program depends on a modified version of '
        r'this [^ ]+ you (must|are encouraged to) publicly release the '
        r'modified version of this',
        'EFL',
        lambda t, m, l:
            'EFL-{}'.format('1' if m.group(1).lower() == 'must' else '2')
    ),
    # Expat
    ReLicense(
        r'MIT license:? '
        r'\(?https?[: ]/?/?www\.opensource\.org/licenses/mit-license\.php\)?',
        'MIT/X11',
        lambda *a: 'Expat'),
    ReLicense(
        r'licensed under the (?:terms of either the ?)?MIT.* '
        r'\(?https?[: ]/?/?www\.opensource\.org/licenses/mit-license\.php\)?',
        'MIT/X11',
        lambda *a: 'Expat'),
    ReLicense(
        r'Some rights reserved:? '
        r'\(?https?[: ]/?/?www\.opensource\.org/licenses/mit-license\.php\)?',
        'MIT/X11',
        lambda *a: 'Expat'),
    ReLicense(
        r'License(?: name)?:? MIT url:? '
        r'\(?https?[: ]/?/?www\.opensource\.org/licenses/mit-license\.php\)?',
        'MIT/X11',
        lambda *a: 'Expat'),
    # as used by bluez-alsa
    ReLicense(
        r'This project is licensed under the terms of the MIT license',
        'MIT/X11',
        lambda *a: 'Expat'),
    # as used by sddm
    ReLicense(
        r'SPDX-License-Identifier:? MIT '
        r'\(?https?://spdx\.org/licenses/MIT\.html\)',
        'MIT/X11',
        lambda *a: 'Expat'),
    ReLicense(
        r'The above copyright notice and this permission notice '
        r'shall be included in all copies or substantial portions '
        r'of the (?:Software|Materials?)\. '
        r'The Software shall be used for Good, not Evil\.',
        'MIT/X11',
        lambda *a: 'JSON'),
    ReLicense(
        r'Permission is hereby granted, free of charge, to any '
        r'person obtaining a copy of this software and(?:/or)? '
        r'associated documentation files \(the[" ]*'
        r'(?:Software|Materials)[" ]?\), to deal in the '
        r'(?:Software|Materials) without restriction, including '
        r'without limitation the rights to use, copy, modify, '
        r'merge, publish, distribute, sublicense, and(?:/or)? '
        r'sell copies of the (?:Software|Materials?), and to '
        r'permit persons to whom the (?:Software|Materials?) '
        r'(?:is|are) furnished to do so, subject to the following '
        r'conditions:? '
        r'The above copyright notice and this permission notice '
        r'shall be included in all copies or substantial portions '
        r'of the (?:Software|Materials?)\. '
        r'THE '
        r'(?:[^ ]+ ){,256}'
        r'Except as contained in this notice, the name '
        r'(?:[^ ]+ ){,8}'
        r'shall not be used in advertising or otherwise to promote the '
        r'sale, use or other dealings in this Software without prior '
        r'written authorization from (?:[^ ]+ ){,4}.',
        'MIT/X11',
        lambda *a: 'X11'),
    ReLicense(
        r'Permission is hereby granted, free of charge, to any '
        r'person obtaining a copy of this software and(?:/or)? '
        r'associated documentation files \(the[" ]*'
        r'(?:Software|Materials)[" ]?\), to deal in the '
        r'(?:Software|Materials) without restriction, including '
        r'without limitation the rights to use, copy, modify, '
        r'merge, publish, distribute, sublicense, and(?:/or)? '
        r'sell copies of the (?:Software|Materials?), and to '
        r'permit persons to whom the (?:Software|Materials?) '
        r'(?:is|are) furnished to do so, subject to the following '
        r'conditions:? '
        r'The above copyright notice and this permission notice '
        r'shall be included in all copies or substantial portions '
        r'of the (?:Software|Materials?)\. '
        r'THE ',
        'MIT/X11',
        lambda *a: 'Expat'),
    # GPL
    ReLicense(
        r'GNU General Public License Usage '
        r'(?:[^ ]+ ){,2}This [^ ]+ may be used under the terms of '
        r'the GNU General '
        r'Public License version 2\.0 or 3\.0 '
        r'.{,512} Qt GPL Exception version 1\.3',
        'GPL',
        lambda *a: 'GPL-2+3 with QT-1.3 exception'),
    ReLicense(
        r'GNU General Public License Usage '
        r'(?:[^ ]+ ){,2}This [^ ]+ may be used under the terms of '
        r'the GNU General '
        r'Public License version 2[.0]* or \(at your option\) '
        r'the GNU General Public license version 3[.0]* or any later '
        r'version approved by the KDE Free',
        'GPL',
        lambda *a: 'GPL-2+3+KDEeV'),
    ReLicense(
        r'GNU General Public License Usage '
        r'(?:[^ ]+ ){,2}This [^ ]+ may be used under the terms of '
        r'the GNU General '
        r'Public License version ([\d.]+) (or later)?'
        r'.{,512} (Qt GPL Exception version 1\.3)?',
        'GPL',
        lambda t, m, l:
            'GPL-{}{}{}'.format(
                m.group(1).rstrip('0.'),
                '+' if m.groups()[1] else '',
                ' with QT-1.3 exception' if m.groups()[2] else '')
    ),
    ReLicense(
        r'you can (?:re)?distribute '
        r'(?:it|them) and(?:/or)? (?:[^ ]+ ){,10}modify (?:it|them)'
        r'under the terms of (?:the )? '
        r'(?:version [^ ]+ (?:or later )?(?:\(?only\)? )?of )?'
        r'the (?:GNU )?General Public License',
        'GPL'),
    ReLicense(
        r'you may (?:re)?distribute '
        r'(?:it|them) and(?:/or)? (?:[^ ]+ ){,10}modify (?:it|them)'
        r'under the terms of (?:the )? '
        r'(?:version [^ ]+ (?:or later )?(?:\(?only\)? )?of )?'
        r'the (?:GNU )?General Public License',
        'GPL'),
    ReLicense(
        r'you can (?:re)?distribute '
        r'(?:it|them) and(?:/or)? (?:[^ ]+ ){,10}modify (?:it|them)'
        r'under the terms of (?:the )? '
        r'(?:version [^ ]+ (?:or later )?(?:\(?only\)? )?of )?'
        r'the (?:GNU )?GPL',
        'GPL'),
    ReLicense(
        r'you may (?:re)?distribute '
        r'(?:it|them) and(?:/or)? (?:[^ ]+ ){,10}modify (?:it|them)'
        r'under the terms of (?:the )? '
        r'(?:version [^ ]+ (?:or later )?(?:\(?only\)? )?of )?'
        r'the (?:GNU )?GPL',
        'GPL'),
    ReLicense(
        r'is distributed '
        r'under the terms of (?:the )?'
        r'(?:version [^ ]+ (?:or later )?(?:\(?only\)? )?of )?'
        r'the (?:GNU )?General Public License',
        'GPL'),
    ReLicense(
        r'is distributed '
        r'under the terms of (?:the )?'
        r'(?:version [^ ]+ (?:or later )?(?:\(?only\)? )?of )?'
        r'the (?:GNU )?GPL',
        'GPL'),
    ReLicense(
        r'is licensed '
        r'under the terms of (?:the )?'
        r'(?:version [^ ]+ (?:or later )?(?:\(?only\)? )?of )?'
        r'the (?:GNU )?General Public License',
        'GPL'),
    ReLicense(
        r'is licensed '
        r'under the terms of (?:the )?'
        r'(?:version [^ ]+ (?:or later )?(?:\(?only\)? )?of )?'
        r'the (?:GNU )?GPL',
        'GPL'),
    ReLicense(
        r'(?:(?:is free software.? )?you (?:can|may) (?:re)?distribute '
        r'(?:it|them) and(?:/or)? modify (?:it|them)|is (?:distributed|'
        r'licensed)) under the terms of (?:the )?(?:GNU )?'
        r'(?:General Public License|GPL)',
        'GPL'),
    ReLicense(
        r'is distributed (?:[^ ]+ ){,16}terms (?:[^ ]+ ){,64}GPL',
        'GPL'),
    ReLicense(
        r'is distributed under the (?:[^ ]+ ){,64}GPL',
        'GPL'),
    ReLicense(
        r'licensed under the (?:[^ ]+ ){,64}GPL ',
        'GPL'),
    ReLicense(
        r'may be distributed and/or modified under the '
        r'terms of the GNU General Public License',
        'GPL'),
    ReLicense(
        r'You can Freely distribute this program under the GNU '
        r'General Public License',
        'GPL'),
    ReLicense(
        r'GNU GENERAL PUBLIC LICENSE '
        r'Version (\d+), (?:\d+ )?\w+ \d+ '
        r'Copyright \(C\) (?:[\d, ]+) Free Software Foundation, Inc. '
        r'(?:<?https?[: ]/?/?fsf.org/>? |'
        r'51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA )?'
        r'Everyone is permitted to copy and distribute verbatim copies '
        r'of this license document, but changing it is not allowed.',
        'GPL', _name_version),
    ReLicense(
        r'may be used under the terms of the GNU General Public License '
        r'version (\d)\.0',
        'GPL', _name_version),
    ReLicense(
        r'license:? (?:[^ ]+ ){,2}[("]?'
        r'https?[: ]/?/?creativecommons\.org/licenses/GPL/2\.0/?[")]?',
        'GPL',
        lambda *a: 'GPL-2+'),
    ReLicense(
        r'License:? GPLWithACException',
        'GPL',
        lambda *a: 'GPL with AutoConf exception'),
    ReLicense(
        r'License:? GPL[- v]*([\d.]*)(\+?)',
        'GPL',
        lambda t, m, l:
            'GPL{}'.format('-{}{}'.format(m.group(1).rstrip('0.'), m.group(2))
                           if m.groups()[0] else '')
    ),
    ReLicense(
        r'This [^ ]+ is (?:freely distributable|licensed) under '
        r'the (?:GNU (?:General )?Public License|GPL)',
        'GPL'),
    ReLicense(
        r' GPL2 or later ', 'GPL',
        lambda *a: 'GPL-2+'),
    ReLicense(
        r' is Free Software, released under the GNU Public '
        r'License\. ',
        'GPL',
        lambda *a: 'GPL-2+'),
    # mpl alternative license
    ReLicense(
        r'may be used under the terms of '
        r'(?:[^ ]+ ){,20}the GNU General '
        r'Public License '
        r'Version ([\d.]+) ((?:or later)?)',
        'GPL',
        lambda t, m, l:
            'GPL-{}{}'.format(
                m.group(1).rstrip('0.'),
                '+' if m.groups()[1] else '')
    ),
    # License document without the example boiler plate
    ReLicense(
        r'GNU GENERAL PUBLIC LICENSE '
        r'Version ([\d.]+),? (?:[^ ]+ ){,4}'
        r'Copyright (?:[^ ]+ ){,32}'
        r'Everyone is permitted to copy and distribute verbatim copies '
        r'of this license document, but changing it is not allowed\.',
        'GPL', partial(_name_version, name='GPL')),
    # LGPL
    ReLicense(
        r'GNU (?:Lesser|Library) General Public License Usage '
        r'(?:[^ ]+ ){,2}This [^ ]+ may be used under the terms of '
        r'the GNU (?:Lesser|Library) General '
        r'Public License version ([\d.]+) (or later)?'
        r'.{,512} (Qt LGPL Exception version ([\d.]+))?',
        'LGPL',
        lambda t, m, l:
            'LGPL-{}{}{}'.format(
                m.group(1).rstrip('0.'),
                '+' if m.groups()[1] else '',
                ' with QT-{} exception'.format(m.group(4))
                if m.groups()[2] else
                '')
    ),
    ReLicense(
        r'GNU Lesser General Public License Usage '
        r'(?:[^ ]+ ){,2}This [^ ]+ may be used under the terms of '
        r'the GNU Lesser General '
        r'Public License version 2\.1 '
        r'.{,512} Qt LGPL Exception version 1\.1',
        'LGPL',
        lambda *a: 'LGPL-2.1 with QT-1.1 exception'),
    ReLicense(
        r'Qwt License Version 1.0,? .+'
        r'The Qwt library and included programs are provided under '
        r'the terms of the GNU LESSER GENERAL PUBLIC LICENSE ',
        'LGPL',
        lambda *a: 'LGPL-2.1 with Qwt-1 exception'),
    ReLicense(
        r'(?:(?:is free software.? )?you (?:can|may) (?:re)?distribute '
        r'(?:it|them) and(?:/or)? modify (?:it|them)|is (?:distributed|'
        r'licensed)) under the terms (?:and conditions )?of (?:the )?'
        r'(?:version [^ ]+ '
        r'(?:\(?only\)? )?of )?the (?:GNU )?'
        r'(?:(?:Library(?:/Lesser)? |Lesser(?:/Library)? )'
        r'(?:GNU )?General Public Licen[sc]e|LGPL)',
        'LGPL'),
    ReLicense(
        r'(?:(?:is free software.? )?you (?:can|may) (?:re)?distribute '
        r'(?:it|them) and(?:/or)? modify (?:it|them)|is (?:distributed|'
        r'licensed)) under the terms of (?:the )?(?:GNU )?'
        r'(?:(?:Library |Lesser(?:/Library)? )(?:GNU )?General Public '
        r'(?:version(?: [^ ]+){,5} )?License|LGPL)',
        'LGPL'),
    ReLicense(
        r'licensed under (?:[^ ]+ ){,10}LGPL ',
        'LGPL'),
    ReLicense(
        r'may be (?:distributed and/or modified|used) under the terms of '
        r'the GNU Lesser(?:/Library)? '
        r'General Public License',
        'LGPL'),
    ReLicense(
        r'This [^ ]+ is '
        r'(?:distributed|freely distributable|licensed) under '
        r'the (?:(?:terms|conditions) of the |license )?(?:GNU )?'
        r'(?:(?:Lesser|Library) (?:General )?'
        r'Public License|LGPL)',
        'LGPL'),
    # mpl alternative license
    ReLicense(
        r'may be used under the terms of '
        r'(?:[^ ]+ ){,20}the GNU (?:Lesser|Library) General '
        r'Public License '
        r'Version ([\d.]+) ((?:or later)?)',
        'LGPL',
        lambda t, m, l:
            'LGPL-{}{}'.format(
                m.group(1).rstrip('0.'),
                '+' if m.groups()[1] else '')
    ),
    ReLicense(
        r'License:? LGPL[- v]*([\d.]*)(\+?)',
        'LGPL',
        lambda t, m, l:
            'LGPL{}'.format(
                '-{}{}'.format(m.group(1).rstrip('0.'), m.group(2))
                if m.groups()[0] and m.group(1).rstrip('0.') else
                '')
    ),
    ReLicense(
        r'Distributed under the LGPL\.',
        'LGPL'),
    ReLicense(
        r'This (?:[^ ]+ ){,2}is free software licensed under the '
        r'GNU LGPL(?:\.|->) You can find a copy of this license in '
        r'LICENSE\.txt in the top directory of the source '
        r'code(\.|->)',
        'LGPL'),  # found in marble, actually lgpl-2.1+
    ReLicense(
        r'This version of the GNU Lesser General Public License '
        r'incorporates the terms and conditions of version 3 of the '
        r'GNU General Public License, supplemented by the additional '
        r'permissions listed below.',
        'LGPL',
        lambda *a: 'LGPL-3+'),
    # GFDL, GFDL-NIV
    ReLicense(
        r'Permission is (?:hereby )?granted to copy, distribute '
        r'and(?:/or)? modify this [^ ]+ under the terms of the GNU '
        r'Free Documentation License',
        'GFDL'),
    ReLicense(
        r'[< ]legalnotice[> ][& ]?FDLNotice;[< ]/legalnotice[> ]',
        'GFDL',
        lambda *a: 'GFDL-1.2+'),
    # LPPL
    ReLicense(
        r'This [^ ]+ (?:(?:can|may)(?: be)?|is) (?:re)?distributed '
        r'and(?:/or)? modified under the (?:terms|conditions) of '
        r'the LaTeX Project Public License',
        'LPPL'),
    # MPL
    # cpal is based on the mpl 1.1
    ReLicense(
        r'The contents of this file are subject to the Common '
        r'Public Attribution License Version ([\d.]+) \(the '
        r'(?:“|")?License(?:”|[" ])?\);? you may not use '
        r'this file except in compliance with the License\.',
        'MPL', partial(_name_version, name='CPAL')),
    # erlpl is based on the mpl 1.0
    ReLicense(
        r'The contents of this file are subject to the Erlang '
        r'Public License,? Version ([\d.]+),? \(the '
        r'(?:“|")?License(?:”|[" ])?\);? you may not use '
        r'this file except in compliance with the License\.',
        'MPL', partial(_name_version, name='ErlPL')),
    # interbase is based on the mpl 1.1
    ReLicense(
        r'The contents of this file are subject to the Interbase Public '
        r'License Version ([\d.]+),? \(the (?:“|")?License(?:”|[" ])?\);? '
        r'you may not use this file except in compliance with the '
        r'License\.',
        'MPL', partial(_name_version, name='Interbase')),
    # Hack to avoid the license listing in LiLiQ-R to match the MPL catch all
    ReLicense(
        r'Licence Libre du Québec –? ?Réciprocité \(LiLiQ-R\) '
        r'Version ([\d.]+) '
        r'1\. Préambule ',
        'MPL', partial(_name_version, name='LiLiQ-R')),
    ReLicense(
        r'This Source Code Form is subject to the terms of the '
        r'Mozilla Public License, (?:v\.|version) ([\d.]+)\. '
        r'If a copy of the MPL was not distributed with this '
        r'file, You can obtain one at '
        r'https?[: ]/?/?mozilla.org/MPL/[\d.]+/?\. '
        r'This Source Code Form is [“"]?Incompatible With '
        r'Secondary Licenses[”" ]?, as defined by the Mozilla Public '
        r'License, (?:v.|version) [\d.]+\.',
        'MPL',
        lambda t, m, l:
            'MPL-{} with no-copyleft exception'.format(m.group(1).rstrip('.0'))
    ),
    ReLicense(
        r'The contents of this file are subject to the Netscape Public '
        r'License Version ([\d.]+) \(the "?License[" ]\);? you may not '
        r'use this file except in compliance with the License\. You may '
        r'obtain a copy of the License at '
        r'https?[: ]/?/?www.mozilla.org/NPL/? '
        r'Software distributed under the License is distributed on an '
        r'"?AS IS"? basis, WITHOUT WARRANTY OF ANY KIND, either express '
        r'or implied\. See the License for the specific language '
        r'governing rights and limitations under the License\. '
        r'(?:[^\s]+ ){,8192}'
        r'The contents of this file are subject to the Mozilla Public '
        r'License Version ([\d.]+) \(the "?License[" ]\);? you may not '
        r'use this file except in compliance with the License\. You may '
        r'obtain a copy of the License at '
        r'https?[: ]/?/?www.mozilla.org/MPL/? '
        r'Software distributed under the License is distributed on an '
        r'"?AS IS"? basis, WITHOUT WARRANTY OF ANY KIND, either express '
        r'or implied\. See the License for the specific language '
        r'governing rights and limitations under the License\.',
        'MPL',
        lambda t, m, l:
            'MPL-{1}_NPL-{0}'.format(
                m.group(1).rstrip('.0'), m.group(2).rstrip('.0'))
    ),
    ReLicense(
        r'The contents of this file are subject to the Mozilla Public '
        r'License Version ([\d.]+) \(the "?(?:License|MPL)[" ]\);? '
        r'you may not '
        r'use this file except in compliance with the (?:License|MPL)'
        r'(?: or as specified alternatively below)?'
        r'\. You may '
        r'obtain a copy of the (?:License|MPL) at '
        r'https?[: ]/?/?www.mozilla.org/MPL/? '
        r'Software distributed under the (?:License|MPL) is distributed '
        r'on an '
        r'"?AS IS"? basis, WITHOUT WARRANTY OF ANY KIND, either express '
        r'or implied\. See the (?:License|MPL) for the specific language '
        r'governing rights and limitations under the (?:License|MPL)\.',
        'MPL', _name_version),
    ReLicense(
        r'Mozilla Public License,? (?:Version|v\.?) ?([\d+]+)\.? '
        r'If a copy of the MPL was not distributed with this file, '
        r'You can obtain one at https?[: ]/?/?mozilla.org/MPL/[\d.]+//?',
        'MPL', _name_version),
    # catch dual licensed
    ReLicense(
        r'(?:licensed|released) under the (?:[^ ]+ ){,32}'
        r'Mozilla Public License,? (?:\(?MPL\)? )?v(?:ersion)?\.? ?([\d.]+)?',
        'MPL', _name_version),
    ReLicense(
        r'may be used under the terms of '
        r'(?:either )?the (?:[^ ]+ ){,32}'
        r'Mozilla Public License,? (?:\(?MPL\)? )?v(?:ersion)?\.? ?([\d.]+)?',
        'MPL', _name_version),
    ReLicense(
        r'is distributed under the terms of '
        r'(?:either )?(?:[^ ]+ ){,32}the '
        r'Mozilla Public License,? (?:\(?MPL\)? )?v(?:ersion)?\.? ?([\d.]+)?',
        'MPL', _name_version),
    ReLicense(
        r'Licensed? ?:(?:: OSI Approved ::)? ?'
        r'Mozilla Public License,? (?:\(?MPL\)? )?v(?:ersion)?\.? ?([\d.]+)?',
        'MPL', _name_version),
    ReLicense(
        r'is made available subject to the terms of the '
        r'Mozilla Public License,? (?:\(?MPL\)? )?v(?:ersion)?\.? ?([\d.]+)?',
        'MPL', _name_version),
    ReLicense(
        r'is subject to the terms of the '
        r'Mozilla Public License,? (?:\(?MPL\)? )?v(?:ersion)?\.? ?([\d.]+)?',
        'MPL', _name_version),
    ReLicense(
        r'is to be made available under the terms of the '
        r'Mozilla Public License,? (?:\(?MPL\)? )?v(?:ersion)?\.? ?([\d.]+)?',
        'MPL', _name_version),
    ReLicense(
        r'permits you to use, modify, and distribute this file '
        r'in accordance with the terms of the '
        r'Mozilla Public License,? (?:\(?MPL\)? )?v(?:ersion)?\.? ?([\d.]+)?',
        'MPL', _name_version),
    ReLicense(
        r'the contents of this file '
        r'(?:may be used|are also (?:re)?distributable) under the terms of '
        r'(?:either )? the '
        r'Mozilla Public License,? (?:\(?MPL\)? )?v(?:ersion)?\.? ?([\d.]+)?',
        'MPL', _name_version),
    ReLicense(
        r'is free software and is available to be redistributed and/or '
        r'modified under the terms of (?:either )?the (?:[^ ]+ ){,32}'
        r'Mozilla Public License,? (?:\(?MPL\)? )?v(?:ersion)?\.? ?([\d.]+)?',
        'MPL', _name_version),
    ReLicense(
        r'use under the restrictions of the (?:[^ ]+ ){,32}'
        r'Mozilla Public License,? (?:\(?MPL\)? )?v(?:ersion)?\.? ?([\d.]+)?',
        'MPL', _name_version),
    #
    ReLicense(
        r'you can redistribute it and/or modify it under the terms of '
        r'(?:one or more of the following,? )?(?:[^\s]+ ){,64}'
        r'Mozilla Public License as published by the The Mozilla '
        r'Foundation;? version ([\d.]+)',
        'MPL', _name_version),
    # Perl
    ReLicense(
        r'you can redistribute it and/or modify it under the '
        r'same terms as Perl',
        'GPL',
        lambda *a: 'GPL-1+'),
    ReLicense(
        r'you can redistribute it and/or modify it under the '
        r'same terms as Perl',
        'Artistic',
        lambda *a: 'Artistic-1-Perl'),
    # Python
    ReLicense(
        r'(?:licensed under the'
        r'|released under'
        r'|according to the terms of the'
        r'|license:) '
        r'PYTHON SOFTWARE FOUNDATION LICENSE( VERSION ([^ ]+))?',
        'Python',
        lambda t, m, l:
            'Python{}'.format('-{}'.format(m.group(2))
                              if m.groups()[1] else
                              '')
    ),
    ReLicense(
        r'PYTHON SOFTWARE FOUNDATION LICENSE( VERSION ([^ ]+))? '
        r'See '
        r'https?[: ]/?/?www.opensource.org/licenses/Python-[\d.]+ '
        r'for full terms',
        'Python',
        lambda t, m, l:
            'Python{}'.format('-{}'.format(m.group(2))
                              if m.groups()[1] else
                              '')
    ),
    ReLicense(
        r'PYTHON SOFTWARE FOUNDATION LICENSE( VERSION ([^ ]+))? '
        r'([^\s]+ ){,16}'
        r'This LICENSE AGREEMENT is between the Python Software '
        r'Foundation',
        'Python',
        lambda t, m, l:
            'Python{}'.format('-{}'.format(m.group(2))
                              if m.groups()[1] else
                              '')
    ),
    # QPL
    ReLicense(
        r'may be distributed under the terms '
        r'of the Q Public License( version ([\d.]+))',
        'QPL',
        lambda t, m, l:
            'QPL{}'.format('-{}'.format(m.group(2)).rstrip('.0')
                           if m.groups()[1] else '')
    ),
    ReLicense(
        r'may redistribute it under the terms '
        r'of the Q Public License( version ([\d.]+))',
        'QPL',
        lambda t, m, l:
            'QPL{}'.format('-{}'.format(m.group(2)).rstrip('.0')
                           if m.groups()[1] else '')
    ),
    ReLicense(
        r'This file is part of the .*Qt GUI Toolkit. This file may '
        r'be distributed under the terms of the Q Public License '
        r'as defined',
        'QPL'),
    # W3C
    ReLicense(
        r'This [^ ]+ (?:(?:may|can)(?: be)?|is) (?:re)distributed '
        r'under the (?:W3C®|W3C) Software License',
        'W3C'),
    # Zlib
    # Eurosym license extends zlib
    ReLicense(
        r'The origin of this software must not be misrepresented'
        r'.*Altered source versions must be plainly marked as such'
        r'.*You must not use any of the names of the authors '
        r'or copyright holders of the original software for '
        r'advertising or publicity pertaining to distribution '
        r'without specific, written prior permission'
        r'.*If you change this software and redistribute parts '
        r'or all of it in any form, you must make the source '
        r'code of the altered version of this software available'
        r'.*This notice may not be removed or altered from any '
        r'source distribution',
        'Zlib',
        lambda *a: 'Eurosym'),
    # cube license extends zlib
    ReLicense(
        r'The origin of this software must not be misrepresented'
        r'.*Altered source versions must be plainly marked as such'
        r'.*This notice may not be removed or altered from any '
        r'source distribution'
        r'.*Source versions may not be "?relicensed"? '
        r'under a different license without my explicitly '
        r'written permission\.',
        'Zlib',
        lambda *a: 'Cube'),
    ReLicense(
        r'The origin of this software must not be misrepresented;? '
        r'you must not claim that you wrote the original '
        r'software\. If you use this software in a product, an '
        r'acknowledgment (?:\(see the following\) )?in the '
        r'product documentation is required\.'
        r'.*Altered source versions must be plainly marked as such'
        r'.*This notice may not be removed or altered from any '
        r'source distribution',
        'Zlib',
        lambda *a: 'Zlib-acknowledgement'),
    ReLicense(
        r'The origin of this software must not be misrepresented'
        r'.*Altered source versions must be plainly marked as such'
        r'.*This notice may not be removed or altered from any '
        r'source distribution',
        'Zlib'),
    ReLicense(
        r'see copyright notice in zlib\.h',
        'Zlib'),
    ReLicense(
        r'This code is released under the libpng license',
        'Zlib',
        lambda *a: 'Libpng'),
    # Zope
    ReLicense(
        r'This [^ ]+ (?:(?:(?:can|may)(?: be)?|is) (?:re)?distributed '
        r'and(?:/or)? modified under the (?:terms|conditions)|is '
        r'subject to the provisions) of the Zope Public License',
        'ZPL'),
    ReLicense(
        r'Zope Public License \(?ZPL\)?',
        'ZPL'),

    # Other licenses not in:
    # https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
    ReLicense(
        r'Permission is granted to make and distribute '
        r'verbatim copies of this document provided that '
        r'the copyright notice and this permission notice '
        r'are preserved on all copies. '
        r'Permission is granted to copy and distribute '
        r'modified versions of this document under the '
        r'conditions for verbatim copying, provided that '
        r'the entire resulting derived work is distributed '
        r'under the terms of a permission notice identical '
        r'to this one.',
        'Abstyles',
        lambda *a: 'Abstyles'),
    ReLicense(
        r'Licensed under the Academic Free License version '
        r'([\d.]+)',
        'Academic', _name_version),
    ReLicense(
        r'This file may be freely copied and redistributed '
        r'as long as:? '
        r'(?:[^\s]+ ){,2}'
        r'This entire notice continues to be included in '
        r'the file[,.]? '
        r'(?:[^\s]+ ){,2}'
        r'If the file has been modified in any way, a notice '
        r'of such modification is conspicuously indicated.',
        'Adobe',
        lambda *a: 'Afmparse'),
    ReLicense(
        r'Permission is hereby granted, free of charge, '
        r'to any person obtaining a copy of this documentation '
        r'file, to create their own derivative works from the '
        r'content of this document to use, copy, publish, '
        r'distribute, sublicense, and\/or sell the derivative '
        r'works, and to permit others to do the same, provided '
        r'that the derived work is not represented as being '
        r'a copy or version of this document. '
        r'Adobe shall not be liable to any party for any loss ',
        'Adobe',
        lambda *a: 'Adobe-Glyph'),
    ReLicense(
        r'Adobe Systems Incorporated grants to you a perpetual, '
        r'worldwide, non-exclusive, no-charge, royalty-free, '
        r'irrevocable copyright license, to reproduce, prepare '
        r'derivative works of, publicly display, publicly '
        r'perform, and distribute this source code and such '
        r'derivative works in source or object code form without '
        r'any attribution requirements\.',
        'Adobe',
        lambda *a: 'Adobe-2006'),
    ReLicense(
        r'This file and the (?:\d+ )?PostScript[\( ]?R\)? '
        r'AFM files it accompanies may be used, copied, and '
        r'distributed for any purpose and without charge, '
        r'with or without modification, provided that all '
        r'copyright notices are retained;? that the AFM '
        r'files are not distributed without this file;? '
        r'that all modifications to this file or any of '
        r'the AFM files are prominently noted in the modified '
        r'file[\( ]?s[\) ]?;? and that this paragraph is '
        r'not modified. Adobe Systems has no responsibility '
        r'or obligation to support the use of the AFM files.',
        'Adobe',
        lambda *a: 'APAFML'),
    ReLicense(
        r'This software code is made available "?AS IS"? '
        r'without warranties of any kind. You may copy, '
        r'display, modify and redistribute the software '
        r'code either by itself or as incorporated into '
        r'your code; provided that you do not remove any '
        r'proprietary notices. Your use of this software '
        r'code is at your own risk and you waive any claim '
        r'against Amazon Digital Services, Inc. or its '
        r'affiliates with respect to your use of this software '
        r'code.',
        'ADSL'),
    ReLicense(
        r'(?:(?:is free software.? )?you (?:can|may) (?:re)?distribute '
        r'(?:it|them) and(?:/or)? modify (?:it|them)|is (?:distributed|'
        r'licensed)) under the terms of (?:the )?(?:version [^ ]+ '
        r'(?:\(?only\)? )?of )?the (?:GNU )?'
        r'(?:Affero (?:GNU )?General Public License|AGPL)',
        'AGPL'),
    ReLicense(
        r'(?:(?:is free software.? )?you (?:can|may) (?:re)?distribute '
        r'(?:it|them) and(?:/or)? modify (?:it|them)|is (?:distributed|'
        r'licensed)) under the terms of (?:the )?(?:GNU )?'
        r'(?:Affero (?:GNU )?General Public License|AGPL)',
        'AGPL'),
    ReLicense(
        r'The licenses for most software are designed to take away your '
        r'freedom to share and change it. By contrast, the Affero '
        r'General Public License is intended to guarantee your freedom '
        r'to share and change free software[- ]?-?to make sure the '
        r'software '
        r'is free for all its users. This Public License applies to '
        r'most '
        r'of Affero[\' ]?s software and to any other program whose '
        r'authors '
        r'commit to using it. \(?Some other Affero software is covered by '
        r'the GNU Library General Public License instead.\)? You can '
        r'apply '
        r'it to your programs, too.',
        'AGPL',
        lambda *a: 'AGPL-1'),
    ReLicense(
        r'Aladdin Enterprises hereby grants to anyone the '
        r'permission to apply this License to their own '
        r'work, as long as the entire License \(?including '
        r'the above notices and this paragraph\)? is copied '
        r'with no changes, additions, or deletions except '
        r'for changing the first paragraph of Section 0 '
        r'to include a suitable description of the work '
        r'to which the license is being applied and of '
        r'the person or entity that holds the copyright '
        r'in the work, and, if the License is being applied '
        r'to a work created in a country other than the '
        r'United States, replacing the first paragraph '
        r'of Section 6 with an appropriate reference to '
        r'the laws of the appropriate country.',
        'Aladdin'),
    ReLicense(
        r'Redistribution and use in any form of this material '
        r'and any product thereof including software in '
        r'source or binary forms, along with any related '
        r'documentation, with or without modification '
        r'\(?[" ]this material[" ]\)?, is permitted provided '
        r'that the following conditions are met:? '
        r'(?:[^\s]+ ){,80}'
        r'Neither the names nor trademarks of (?:[^\s]+ ){,8}'
        r'(?:or )?any copyright holders or contributors may '
        r'be used to endorse or promote products derived '
        r'from this material without specific prior written '
        r'permission.',
        'AMDPLPA'),
    ReLicense(
        r'In consideration of your agreement to abide by '
        r'the following terms, and subject to these terms, '
        r'Apple grants you a personal, non-exclusive license, '
        r'under Apple[\' ]?s copyrights in this original '
        r'Apple software \(?the "?Apple Software[" ]?\)?, '
        r'to use, reproduce, modify and redistribute the '
        r'Apple Software, with or without modifications, '
        r'in source and[/ ]or binary forms;? provided that '
        r'if you redistribute the Apple Software in its '
        r'entirety and without modifications, you must '
        r'retain this notice and the following text and '
        r'disclaimers in all such redistributions of the '
        r'Apple Software. Neither the name, trademarks, '
        r'service marks or logos of Apple Computer, Inc. '
        r'may be used to endorse or promote products derived '
        r'from the Apple Software without specific prior '
        r'written permission from Apple. Except as expressly '
        r'stated in this notice, no other rights or licenses, '
        r'express or implied, are granted by Apple herein, '
        r'including but not limited to any patent rights '
        r'that may be infringed by your derivative works '
        r'or by other works in which the Apple Software '
        r'may be incorporated.',
        'AML'),
    ReLicense(
        r'We reserve no legal rights to the ANTLR-?[- ]?'
        r'it is fully in the public domain. An individual '
        r'or company may do whatever they wish with source '
        r'code distributed with ANTLR or the code generated '
        r'by ANTLR, including the incorporation of ANTLR, '
        r'or its output, into commerical software.',
        'public-domain',
        lambda *a: 'ANTLR-PD'),
    ReLicense(
        r'THE LICENSED WORK IS PROVIDED UNDER THE TERMS OF '
        r'(?:THIS|THE) ADAPTIVE PUBLIC LICENSE '
        r'\(?[" ]?LICENSE[" ]?[\) ]?',
        'APL',
        lambda *a: 'APL-1'),
    ReLicense(
        r'APPLE PUBLIC SOURCE LICENSE '
        r'Version ([\d.]+) - \w+ \d+, \d+ '
        r'Please read this License carefully before downloading this '
        r'software\. By downloading (?:and|or) using this software, '
        r'you are '
        r'agreeing to be bound by the terms of this License\. If you do '
        r'not or cannot agree to the terms of this License, please do '
        r'not download or use the software.',
        'APSL', _name_version),
    ReLicense(
        r'This file contains Original Code and/or Modifications of '
        r'Original Code as defined in and that are subject to the Apple '
        r'Public Source License Version ([\d.]+) \(the \'?License[\' ]?\). '
        r'You may not use this file except in compliance with the '
        r'License\. Please obtain a copy of the License at '
        r'.+ '
        r'and read it before using this file.',
        'APSL', _name_version),
    ReLicense(
        r'These patterns were developed for internal GMV '
        r'use and are made public in the hope that they '
        r'will benefit others. Also, spreading these patterns '
        r'throughout the Spanish[- ]language TeX community '
        r'is expected to provide back[- ]benefits to GMV in '
        r'that it can help keeping GMV in the mainstream '
        r'of spanish users.',
        'Bahyph'),
    ReLicense(
        r'Its use is unrestricted. It may be freely distributed, '
        r'unchanged, for non[- ]commercial or commercial '
        r'use. If changed, it must be renamed. Inclusion '
        r'in a commercial software package is also permitted, '
        r'but I would appreciate receiving a free copy for '
        r'my personal examination and use.',
        'Barr'),
    ReLicense(
        r'The contents of this file are subject to the '
        r'BitTorrent Open Source License Version ([\d.]+) \(the '
        r'License\). You may not copy or use this file, '
        r'in either source code or executable form, except '
        r'in compliance with the License.',
        'BitTorrent', _name_version),
    ReLicense(
        r'You may freely use, modify, and/or distribute '
        r'each of the files in this package without limitation. '
        r'The package consists of the following files:?'
        r'.+'
        r'Of course no support is guaranteed',
        'Borceux'),
    ReLicense(
        r'License: BSD Protection license',
        'BSD',
        lambda *a: 'BSD-protection'),
    ReLicense(
        r'Boost Software License[ .,-]+(Version (\d+(?:\.\d+)*)[ .,-]+?)?',
        'BSL',
        lambda t, m, l:
            'BSL{}'.format('-{}'.format(m.group(2).rstrip('.0'))
                           if m.groups()[1] else
                           '')
    ),
    ReLicense(
        r'Permission is hereby granted, free of charge, to any '
        r'person or organization obtaining a copy of the software '
        r'and accompanying documentation covered by this license '
        r'\(the[" ]*Software[" ]?\)',
        'BSL'),
    ReLicense(
        r'THE BEER-WARE LICENSE',
        'Beerware'),
    ReLicense(
        r'hereby grants a fee free license that includes '
        r'the rights use, modify and distribute this named '
        r'source code, including creating derived binary '
        r'products created from the source code.',
        'Caldera'),
    ReLicense(
        r'Computer Associates Trusted Open Source License '
        r'(?:Version )?1\.1',
        'CATOSL',
        lambda *a: 'CATOSL-1.1'),
    ReLicense(
        r'Ce contrat est une licence de logiciel libre '
        r'dont l\'objectif est de conférer aux utilisateurs '
        r'la liberté de modification et de redistribution '
        r'du logiciel régi par cette licence dans le cadre '
        r'd\'un modèle de diffusion '
        r'(?:en logiciel libre'
        r'|(?:« ?)?open source ?»? ?'
        r'(?: fondée sur le droit français)?)\.',
        'CeCILL'),
    ReLicense(
        r'The purpose of this Free Software Licensing Agreement '
        r'is to grant users the right to modify and redistribute '
        r'the software governed by this license within the '
        r'framework of an ["«]? ?open source ?["»]? '
        r'distribution model\. ',
        'CeCILL'),
    ReLicense(
        r'Ce contrat est une licence de logiciel libre '
        r'dont l\'objectif est de conférer aux utilisateurs '
        r'une très large liberté de modification et de '
        r'redistribution du logiciel régi par cette licence\. ',
        'CeCILL',
        lambda *a: 'CeCILL-B'),
    ReLicense(
        r'This Agreement is an open source software license '
        r'intended to give users significant freedom to '
        r'modify and redistribute the software licensed '
        r'hereunder\. ',
        'CeCILL',
        lambda *a: 'CeCILL-B'),
    ReLicense(
        r'Ce contrat est une licence de logiciel libre '
        r'dont l\'objectif est de conférer aux utilisateurs '
        r'la liberté de modifier et de réutiliser le logiciel '
        r'régi par cette licence\. ',
        'CeCILL',
        lambda *a: 'CeCILL-C'),
    ReLicense(
        r'The purpose of this Free Software license agreement '
        r'is to grant users the right to modify and re[- ]?use '
        r'the software governed by this license\. ',
        'CeCILL',
        lambda *a: 'CeCILL-C'),
    ReLicense(
        r'under the terms of the CeCILL-([^ ]+) ',
        'CeCILL', _name_version),
    ReLicense(
        r'under the terms of the CeCILL ',
        'CeCILL'),
    ReLicense(
        r'is made available subject to the terms and conditions '
        r'in CNRI\'?s License Agreement\.',
        'CNRI'),
    ReLicense(
        r'IS PROVIDED UNDER THE TERMS OF THIS CODE PROJECT '
        r'OPEN LICENSE',
        'CPOL',
        lambda *a: 'CPOL-1.02'),
    ReLicense(
        r'is distributed in the hope that it will be useful, '
        r'but WITHOUT ANY WARRANTY\. No author or distributor '
        r'accepts responsibility to anyone for the consequences '
        r'of using it or for whether it serves any particular '
        r'purpose or works at all, unless he says so in '
        r'writing\. '
        r'Everyone is granted permission to copy, modify '
        r'and redistribute .+, provided this copyright notice '
        r'is preserved and any modifications are indicated\.',
        'Crossword'),
    ReLicense(
        r'The module is,?(?: [^ ]+){,5} licensed under(:? [^ ]+){,2} '
        r'CRYPTOGAMS(?: [^ ]+){,16} https?[: ]/?/?www.openssl.org/[~ ]?appro/cryptogams',
        'CRYPTOGAMS'),
    ReLicense(
        r'is freeware. This means you can pass copies around '
        r'freely provided you include this document in it\'s '
        r'original form in your distribution\.',
        'CrystalStacker'),
    ReLicense(
        r'The contents of this file are subject to the '
        r'CUA Office Public License Version ([\d.]+)',
        'CUA-OPL', _name_version),
    ReLicense(
        r'Dieses Programm kann durch jedermann gemäß den '
        r'Bestimmungen der Deutschen Freien Software Lizenz '
        r'genutzt werden\.',
        'D-FSL'),
    ReLicense(
        r'is open-source, freely available software, you '
        r'are free to use, modify, copy, and distribute--perpetually '
        r'and irrevocably--',
        'DOC'),
    ReLicense(
        r'This file may be freely transmitted and reproduced, '
        r'but it may not be changed unless the name is changed '
        r'also (?:\(except that you may freely change the '
        r'paper-size option for \\?documentclass\))?\. '
        'This notice must be left intact\.',
        'Dotseqn'),
    ReLicense(
        r'This program discloses material protectable under '
        r'copyright laws of the United States\. Permission '
        r'to copy and modify this software and its documentation '
        r'is hereby granted, provided that this notice is '
        r'retained thereon and on all copies or modifications\. ',
        'DSDP'),
    ReLicense(
        r'A modified version of this file may be distributed, '
        r'but it should be distributed with a \*?different\*? '
        r'name\. Changed files must be distributed \*?together '
        r'with a complete and unchanged\*? distribution of '
        r'these files\.',
        'dvipdfm'),
    ReLicense(
        r'1. you can do what you want with it '
        r'2. I refuse any responsibility for the consequences',
        'Permissive',
        lambda *a: 'diffmark'),
    ReLicense(
        r'Licensed under the Educational Community License,? version '
        r'([\d.]+)',
        'ECL', _name_version),
    ReLicense(
        r'Permission is hereby granted to use, copy, modify '
        r'and/or distribute this package, provided that:? '
        r'copyright notices are retained unchanged,? '
        r'any distribution of this package, whether modified '
        r'or not, includes this (?:license text|file)',
        'EFL'),
    ReLicense(
        r'Subject to the terms and conditions '
        r'(?:in|of this) eGenix.com Public License Agreement',
        'CNRI',
        lambda *a: 'eGenix'),
    ReLicense(
        r'Redistribution and use in source and binary forms, '
        r'with or without modification, are permitted provided '
        r'that the following conditions are met:? '
        r'.{,4}Redistributions of source code must retain '
        r'the above copyright notice, this list of conditions '
        r'and the following disclaimer. '
        r'.{,4}Redistributions in binary form must reproduce '
        r'the above copyright notice, this list of conditions '
        r'and the following disclaimer in the documentation '
        r'and/or other materials provided with the distribution. '
        r'.{,4}The end-user documentation included with the '
        r'redistribution, if any, must include the following '
        r'acknowledgment:? '
        r'"?This product includes open source software developed by '
        r'(?:[^ ]+ ){,16}'
        r'Alternately, this acknowledgment may appear in the software '
        r'itself, if and wherever such third-party acknowledgments '
        r'normally appear. '
        r'.{,4}The names? (?:[^ ]+ ){,8}must not be used to '
        r'endorse or promote products derived from this software '
        r'without prior written permission. For written permission, '
        r'please contact (?:[^ ]+ ){,8}'
        r'.{,4}Products derived from this software may not be called '
        r'(?:[^ ]+ ){,8}nor may (?:[^ ]+ ){,8}appear in their name, '
        r'without prior written permission of (?:[^ ]+ ){,4}'
        r'''THIS SOFTWARE IS PROVIDED (?:``|''|")?AS IS(?:''|")? AND '''
        r'ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT '
        r'LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY',
        'BSD',
        lambda *a: 'BSD-like-Entessa'),
    ReLicense(
        r'Redistribution and use in source and binary forms, '
        r'with or without modification, are permitted provided '
        r'that the following conditions are met:? '
        r'.{,4}Redistributions of source code must retain '
        r'the above copyright notice, this list of conditions,? '
        r'and the following disclaimer\. '
        r'.{,4}Redistributions in binary form must reproduce the '
        r'above copyright notice, this list of conditions, and the '
        r'disclaimer that follows these conditions in the documentation '
        r'and/or other materials provided with the distribution\. '
        r'.{,4}The names? (?:[^ ]+ ){,8}must not be used to endorse or '
        r'promote products derived from this software without prior '
        r'written permission\. For written permission, please contact '
        r'(?:[^ ]+ ){,8}'
        r'.{,4}Products derived from this software may not be called '
        r'(?:[^ ]+ ){,8}nor may (?:[^ ]+ ){,8}appear in their name, '
        r'without prior written permission from the (?:[^ ]+ ){,8}'
        r'In addition, we request \(but do not require\) that you include '
        r'in the end-user documentation provided with the '
        r'redistribution and/or in the software itself an '
        r'acknowledgement equivalent to the following:? '
        r'"?This product includes software developed by the (?:[^ ]+ ){,8}'
        r'Alternatively, the acknowledgment may be graphical using the '
        r'logos available at (?:[^ ]+ ){,8}'
        r'''THIS SOFTWARE IS PROVIDED (?:``|''|")?AS IS(?:''|")? AND '''
        r'ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT '
        r'LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY',
        'BSD',
        lambda *a: 'BSD-like-Saxpath'
    ),
    ReLicense(
        r'Eclipse Public License - v ([\d.]+) '
        r'THE ACCOMPANYING PROGRAM IS PROVIDED UNDER THE TERMS OF '
        r'THIS ECLIPSE PUBLIC LICENSE \([" ]AGREEMENT[" ]\)\. ANY USE, '
        r'REPRODUCTION OR DISTRIBUTION OF THE PROGRAM CONSTITUTES '
        r'RECIPIENT[\' ]S ACCEPTANCE OF THIS AGREEMENT\.',
        'EPL', _name_version),
    ReLicense(
        r'published under the EPL ([\d.]+), '
        r'see COPYING\.TESTDATA for the full license',
        'EPL', _name_version),
    ReLicense(
        r'Licensed under the EUPL V.([\d.]+)',
        'EUPL', _name_version),
    ReLicense(
        r'Usage of the works is permitted provided that '
        r'this instrument is retained with the works, so '
        r'that any entity that uses the works is notified '
        r'of this instrument\. '
        r'DISCLAIMER:? THE WORKS ARE WITHOUT WARRANTY\.',
        'Fair'),
    ReLicense(
        r'This file contains software that has been made '
        r'available under The Frameworx Open License 1.0. '
        r'Use and distribution hereof are subject to the '
        r'restrictions set forth therein\.',
        'Frameworx',
        lambda *a: 'Frameworx-1'),
    ReLicense(
        r'The contents of this file are subject to the '
        r'FreeImage Public License Version 1.0 \(the "?License[" ]?\);? '
        r'you may not use this file except in compliance '
        r'with the License\.',
        'FreeImage',
        lambda *a: 'FreeImage-1'),
    ReLicense(
        r'This file (?:is part of the FreeType project, and )?may only be used,? '
        r'modified,? and distributed under the terms of the FreeType project '
        r'license, LICENSE.TXT. By continuing to use, modify, or distribute '
        r'this file you indicate that you have read the license and '
        r'understand and accept it fully.',
        'FTL'),
    ReLicense(
        r'This license applies to all files distributed in the original '
        r'FreeType Project, including all source code, binaries and '
        r'documentation, unless otherwise stated in the file in its '
        r'original, unmodified form as distributed in the original archive. '
        r'If you are unsure whether or not a particular file is covered by '
        r'this license, you must contact us to verify this.',
        'FTL'),
    ReLicense(
        r'In lieu of a licence Fonts in this site are offered free for any '
        r'use; they may be installed, embedded, opened, edited, modified, '
        r'regenerated, posted, packaged and redistributed.',
        'fonts-ancient-scripts_license'),
    ReLicense(
        r'is gift-ware. It was created by a number of people '
        r'working in cooperation, and is given to you freely '
        r'as a gift. You may use, modify, redistribute, and '
        r'generally hack it about in any way you like, and '
        r'you do not have to give us anything in return.',
        'Giftware'),
    ReLicense(
        r'Permission to use, copy, and distribute this '
        r'software and its documentation for any purpose '
        r'with or without fee is hereby granted, provided '
        r'that the copyright notice appear in all copies '
        r'and that both that copyright notice and this '
        r'permission notice appear in supporting documentation. '
        r'Permission to modify and distribute modified versions '
        r'of this software is granted, provided that:? '
        r'.{,4}the modifications are licensed under the '
        r'same terms as this software;? '
        r'.{,4}you make available the source code of any '
        r'modifications that you distribute, either on '
        r'the same media as you distribute any executable '
        r'or other form of this software, or via a mechanism '
        r'generally accepted in the software development '
        r'community for the electronic transfer of data.',
        'GL2PS'),
    ReLicense(
        r'THIS SOFTWARE IS SUBJECT TO COPYRIGHT PROTECTION '
        r'AND IS OFFERED ONLY PURSUANT TO THE 3DFX GLIDE '
        r'GENERAL PUBLIC LICENSE. THERE IS NO RIGHT TO '
        r'USE THE GLIDE TRADEMARK WITHOUT PRIOR WRITTEN '
        r'PERMISSION OF 3DFX INTERACTIVE, INC. A COPY OF '
        r'THIS LICENSE MAY BE OBTAINED FROM THE DISTRIBUTOR '
        r'OR BY CONTACTING 3DFX INTERACTIVE INC \(info@3dfx.com\). '
        r'THIS PROGRAM. IS PROVIDED "?AS IS"? WITHOUT WARRANTY '
        r'OF ANY KIND, EITHER EXPRESSED OR IMPLIED. SEE THE '
        r'3DFX GLIDE GENERAL PUBLIC LICENSE FOR A FULL '
        r'TEXT OF THE NON-WARRANTY PROVISIONS.',
        'Glide'),
    ReLicense(
        r'You may copy and distribute it freely, by any '
        r'means and under any conditions, as long as the '
        r'code and documentation is not changed. You may '
        r'also incorporate this code into your own program '
        r'and distribute that, or modify this code and '
        r'use and distribute the modified version, as long '
        r'as you retain a notice in your program or documentation '
        r'which mentions my name and the URL shown above.',
        'Glulxe'),
    ReLicense(
        r'Permission to use, copy, and distribute this software '
        r'and its documentation for any purpose with or '
        r'without fee is hereby granted, provided that '
        r'the above copyright notice appear in all copies '
        r'and that both that copyright notice and this '
        r'permission notice appear in supporting documentation. '
        r'Permission to modify the software is granted, '
        r'but not the right to distribute the complete '
        r'modified source code. Modifications are to be '
        r'distributed as patches to the released version. '
        r'Permission to distribute binaries produced by '
        r'compiling modified sources is granted, provided you ',
        'gnuplot'),
    ReLicense(
        r'The contents of this file are subject to the '
        r'gSOAP Public License Version 1.3 \(the '
        r'"?License[" ]?\);? you may not use this file '
        r'except in compliance with the License.',
        'gSOAP',
        lambda *a: 'gSOAP-1.3'),
    ReLicense(
        r'The authors intend this Report to belong to the '
        r'entire Haskell community, and so we grant permission '
        r'to copy and distribute it for any purpose, provided '
        r'that it is reproduced in its entirety, including '
        r'this Notice. Modified versions of this Report may '
        r'also be copied and distributed for any purpose, '
        r'provided that the modified version is clearly '
        r'presented as such, and that it does not claim '
        r'to be a definition of the Haskell 2010 Language.',
        'HaskellReport'),
    ReLicense(
        r'This source code has been made available to you by IBM on an '
        r'AS-IS basis. Anyone receiving this source is licensed under '
        r'IBM copyrights to use it in any way he or she deems fit, '
        r'including copying it, modifying it, compiling it, and '
        r'redistributing it either with or without modifications. '
        r'No license under IBM patents or patent applications is to be '
        r'implied by the copyright license. '
        r'Any user of this software should understand that IBM cannot '
        r'provide technical support for this software and will not be '
        r'responsible for any consequences resulting from the use of '
        r'this software. '
        r'Any person who transfers this source code or any derivative '
        r'work must include the IBM copyright notice, this paragraph, '
        r'and the preceding two paragraphs in the transferred software.',
        'IBM-pibs'),
    ReLicense(
        r'These conditions apply to any software derived from or based on the IJG '
        r'code, not just to the unmodified library. If you use our work, you ought '
        r'to acknowledge us.',
        'IJG'),
    ReLicense(
        r'Licensed under the ImageMagick License \(the[" ]*License[" ]?\)'
        r';? you may not use this file except in compliance with the '
        r'License. You may obtain a copy of the License at '
        r'https?[: ]/?/?www.imagemagick.org/script/license.php '
        r'Unless required by applicable law or agreed to in writing, '
        r'software distributed under the License is distributed on an '
        r'(?:``|''|")?AS IS(?:''|")? BASIS, WITHOUT WARRANTIES OR '
        r'CONDITIONS OF ANY KIND, either express or implied. See the '
        r'License for the specific language governing permissions and '
        r'limitations under the License.',
        'ImageMagick'),
    ReLicense(
        r'This license agreement covers your use of the iMatix '
        r'STANDARD FUNCTION LIBRARY \(SFL\), its source code, '
        r'documentation, and executable files, hereinafter referred '
        r'to as[" ]*the Product[" ]?.',
        'iMatix'),
    ReLicense(
        r'This software is provided[" ]*as is,[" ]*without warranty of any '
        r'kind, express or implied. In no event shall Info-ZIP or its '
        r'contributors be held liable for any direct, indirect, '
        r'incidental, special or consequential damages arising out of '
        r'the use of or inability to use this software.',
        'Info-ZIP'),
    ReLicense(
        r'ACPI - Software License Agreement '
        r'Software License Agreement IMPORTANT - READ BEFORE COPYING, '
        r'INSTALLING OR USING\. '
        r'Do not use or load this software and any associated materials '
        r'\(collectively, the [" ]?Software[" ]?\) until you have carefully '
        r'read the following terms and conditions\. By loading or using '
        r'the Software, you agree to the terms of this Agreement. If you '
        r'do not wish to so agree, do not install or use the Software\. '
        r'1\. COPYRIGHT NOTICE Some or all of this work - Copyright '
        r'[© ]*[0-9, -]* Intel Corp\. All rights reserved\. '
        r'2\. LICENSE '
        r'2\.1\. This is your license from Intel Corp\. under its '
        r'intellectual property rights\. You may have additional license '
        r'terms from the party that provided you this software, covering '
        r'your right to use that party\'s intellectual property rights\. '
        r'2\.2\. Intel grants, free of charge, to any person '
        r'\([" ]?Licensee[" ]?\) obtaining a copy of the source code '
        r'appearing in this file \([" ]?Covered Code[" ]?\) an '
        r'irrevocable, perpetual, worldwide license under Intel\'s '
        r'copyrights in the base code distributed originally by Intel '
        r'\([" ]?Original Intel Code[" ]?\) to copy, make derivatives, '
        r'distribute, use and display any portion of the Covered Code in '
        r'any form, with the right to sublicense such rights;? and '
        r'2\.3\. Intel grants Licensee a non-exclusive and '
        r'non-transferable patent license \(with the right to '
        r'sublicense\), under only those claims of Intel patents that '
        r'are infringed by the Original Intel Code, to make, use, sell, '
        r'offer to sell, and import the Covered Code and derivative '
        r'works thereof solely to the minimum extent necessary to '
        r'exercise the above copyright license, and in no event shall '
        r'the patent license extend to any additions to or modifications '
        r'of the Original Intel Code\. No other license or right is '
        r'granted directly or by implication, estoppel or otherwise;? ',
        'Intel-ACPI'),
    ReLicense(
        r'IPA Font License Agreement (?:v ?|version ?)?([\d.]+) '
        r'The Licensor provides the Licensed Program \(as defined in '
        r'Article 1 below\) under the terms of this license agreement '
        r'\([" ]?Agreement[" ]\)\. Any use, reproduction or distribution '
        r'of the Licensed Program, or any exercise of rights under this '
        r'Agreement by a Recipient \(as defined in Article 1 below\) '
        r'constitutes the Recipient\'s acceptance of this Agreement\.',
        'IPA', _name_version),
    ReLicense(
        r'IBM Public License Version ([\d.]+) '
        r'THE ACCOMPANYING PROGRAM IS PROVIDED UNDER THE TERMS OF THIS '
        r'IBM PUBLIC LICENSE \([" ]?AGREEMENT[" ]?\)\. ANY USE, REPRODUCTION OR '
        r'DISTRIBUTION OF THE PROGRAM CONSTITUTES RECIPIENT\'S '
        r'ACCEPTANCE OF THIS AGREEMENT\.',
        'IPL', _name_version),
    ReLicense(
        r'JasPer License Version ([\d.]+) '
        r'(?:[^\s]+ ){,80}'
        r'All rights reserved\. '
        r'Permission is hereby granted, free of charge, to any person '
        r'\(the "?User[" ]?\) obtaining a copy of this software and '
        r'associated documentation files \(the "?Software[" ]?\), to deal '
        r'in the Software without restriction, including without '
        r'limitation the rights to use, copy, modify, merge, publish, '
        r'distribute, and/or sell copies of the Software, and to permit '
        r'persons to whom the Software is furnished to do so, subject to '
        r'the following conditions',
        'JasPer', _name_version),
    ReLicense(
        r'Licence Art Libre (?:\[? ?Copyleft Attitude ?\]? Version )?'
        r'([\d.]+) (?:\(LAL [\d.]+\) )?'
        r'Préambule :? ?'
        r'Avec (?:cette|la) Licence Art Libre, l[’ ]autorisation '
        r'est donnée de '
        r'copier, de diffuser et de transformer librement les '
        r'(?:oeuvres|œuvres) dans le respect des droits de l[’ ]auteur\.',
        'LAL', _name_version),
    ReLicense(
        r'Permission is granted to make and distribute verbatim copies '
        r'of this manual provided the copyright notice and this '
        r'permission notice are preserved on all copies\. '
        r'Permission is granted to copy and distribute modified versions '
        r'of this manual under the conditions for verbatim copying, '
        r'provided that the entire resulting derived work is distributed '
        r'under the terms of a permission notice identical to this one\. '
        r'Permission is granted to copy and distribute translations of '
        r'this manual into another language, under the above conditions '
        r'for modified versions\.',
        'Latex2e'),
    ReLicense(
        r'This software is distributed in the hope that it will be '
        r'useful, but with NO WARRANTY OF ANY KIND\. '
        r'No author or distributor accepts responsibility to anyone for '
        r'the consequences of using this software, or for whether it '
        r'serves any particular purpose or works at all, unless he or '
        r'she says so in writing\. Everyone is granted permission to '
        r'copy, modify and redistribute this source code, for commercial '
        r'or non-commercial purposes, with the following restrictions:? '
        r'\(1\) the origin of this source code must not be '
        r'misrepresented;? '
        r'\(2\) modified versions must be plainly marked as such;? and '
        r'\(3\) this notice may not be removed or altered from any '
        r'source or modified source distribution\.',
        'Leptonica'),
    ReLicense(
        r'Permission to use, copy, modify, distribute, and sell this '
        r'software and its documentation for any purpose is hereby '
        r'granted without fee, provided that \(i\) the above copyright '
        r'notices and this permission notice appear in all copies of '
        r'the software and related documentation, and \(ii\) the names? '
        r'of (?:[^\s]+ ){,80}may not be used in any advertising or '
        r'publicity relating to the software without the specific, prior '
        r'written permission of ',
        'libtiff'),
    ReLicense(
        r'Licence Libre du Québec –? ?Permissive \(LiLiQ-P\) '
        r'Version ([\d.]+) '
        r'1\. Préambule ',
        'LiLiQ-P', _name_version),
    ReLicense(
        r'Licence Libre du Québec –? ?Réciprocité forte '
        r'\(LiLiQ-R\+\) '
        r'Version ([\d.]+) '
        r'1\. Préambule ',
        'LiLiQ-R+', _name_version),
    ReLicense(
        r'Lucent Public License Version ([\d.]+) '
        r'THE ACCOMPANYING PROGRAM IS PROVIDED UNDER THE TERMS OF THIS '
        r'PUBLIC LICENSE \([" ]?AGREEMENT[" ]?\)\. ANY USE, REPRODUCTION '
        r'OR DISTRIBUTION OF THE PROGRAM CONSTITUTES RECIPIENT\'S '
        r'ACCEPTANCE OF THIS AGREEMENT\.',
        'LPL', _name_version),
    ReLicense(
        r'Permission is hereby granted to make and distribute original '
        r'copies of this program provided that the copyright notice and '
        r'this permission notice are preserved and provided that the '
        r'recipient is not asked to waive or limit his right to '
        r'redistribute copies as allowed by this permission notice and '
        r'provided that anyone who receives an executable form of this '
        r'program is granted access to a machine-readable form of the '
        r'source code for this program at a cost not greater than '
        r'reasonable reproduction, shipping, and handling costs\. '
        r'Executable forms of this program distributed without the '
        r'source code must be accompanied by a conspicuous copy of this '
        r'permission notice and a statement that tells the recipient how '
        r'to obtain the source code\.',
        'MakeIndex'),
    ReLicense(
        r'Provided that these terms and disclaimer and all copyright '
        r'notices are retained or reproduced in an accompanying '
        r'document, permission is granted to deal in this work without '
        r'restriction, including unlimited rights to use, publicly '
        r'perform, distribute, sell, modify, merge, give away, or '
        r'sublicence\. '
        r'This work is provided "?AS IS"? and WITHOUT WARRANTY of any '
        r'kind, to the utmost extent permitted by applicable law, '
        r'neither express nor implied;? without malicious intent or '
        r'gross negligence\. In no event may a licensor, author or '
        r'contributor be held liable for indirect, direct, other '
        r'damage, loss, or other issues arising in any way out of '
        r'dealing in the work, even if advised of the possibility of '
        r'such damage or existence of a defect, except proven that it '
        r'results out of said person\'s immediate fault when using the '
        r'work as intended\.',
        'MirOS'),
    # This license seems to be only present in license checkers.. :/
    ReLicense(
        r'MOTOSOTO OPEN SOURCE LICENSE -? ?Version [\d.]+ '
        r'This Motosoto Open Source License \(the "?License[" ]?\) '
        r'applies to "?Community Portal Server"? and related software '
        r'products as well as any updatesor maintenance releases of '
        r'that software \([" ]?Motosoto Products[" ]?\) that are '
        r'distributed by Motosoto\.Com B\.V\. \([" ]?Licensor[" ]\)\. '
        r'Any Motosoto Product licensed pursuant to this License is a '
        r'"?Licensed Product\."? Licensed Product, in its entirety, is '
        r'protected by Dutch copyright law\. This License identifies '
        r'the terms under which you may use, copy, distribute or modify '
        r'Licensed Product and has been submitted to the Open Software '
        r'Initiative \(OSI\) for approval\.',
        'Motosoto'),
    ReLicense(
        r'Permission is hereby granted to use, reproduce, prepare '
        r'derivative works, and to redistribute to others\. '
        r'This software was authored by:? ',
        'mpich'),
    ReLicense(
        r'Microsoft (?:Permissive|Public) License \(Ms-PL\)',
        'MS-PL'),
    ReLicense(
        r'Microsoft Reciprocal License \(Ms-RL\) '
        r'This license governs use of the accompanying software. If you '
        r'use the software, you accept this license\. If you do not '
        r'accept the license, do not use the software\.',
        'MS-RL'),
    ReLicense(
        r'This file is part of the Matrix Template Library '
        r'Dresden University of Technology -?-? ?short TUD -?-? ?and '
        r'Indiana University -?-? ?short IU -?-? ?have the exclusive '
        r'rights to license this product under the following license\.',
        'MTLL'),
    ReLicense(
        r'Permission to use, copy, modify, and distribute these '
        r'programs and their documentation for any purpose and without '
        r'fee is hereby granted, ?provided that the below copyright '
        r'notice and historical background appear in all copies and '
        r'that both the copyright notice and historical background '
        r'and this permission notice appear in supporting '
        r'documentation, and that the names of '
        r'(?:[^\s]+ ){,80}'
        r'not be used in advertising or publicity pertaining to '
        r'distribution of the programs without specific prior written '
        r'permission\.',
        'Multics'),
    ReLicense(
        r'The licenses for most data are designed to take away your '
        r'freedom '
        r'to share and change it. By contrast, this License is intended '
        r'to '
        r'guarantee your freedom to share and change free data[- ]?-?to '
        r'make '
        r'sure the data are free for all their users\. '
        r'This License, the Lesser General Public License for Linguistic '
        r'Resources, applies to some specially designated linguistic '
        r'resources -?-? typically lexicons and grammars\.',
        'LGPL',
        lambda *a: 'LGPLLR'),
    ReLicense(
        r'Permission to use, copy, modify and distribute this '
        r'(?:[^ ]+ ){,3}(?:and its accompanying documentation )?'
        r'for any purpose and without fee is hereby granted'
        r'(?: in perpetuity)?, provided that the above copyright '
        r'notice and this (?:paragraph|permission notice) appear '
        r'in all copies\. The copyright holders make no '
        r'representation about the suitability of (?:[^ ]+ ){,5}'
        r'for any purpose\. It is provided "?as is"? without '
        r'expressed or implied warranty\.',
        'MIT/X11',
        lambda *a: 'MIT_OLIF'),
    ReLicense(
        r'Subject to acceptance of the following conditions, permission '
        r'is hereby granted by Licensors without the need for written '
        r'agreement and without license or royalty fees, to use, copy, '
        r'modify and distribute this software for any purpose\. '
        r'The above copyright notice and the following four paragraphs '
        r'must be reproduced in all copies of this software and any '
        r'software including this software\.',
        'MIT/X11',
        lambda *a: 'MIT_versit'),
    ReLicense(
        r'Permission is hereby granted, free of charge, to any '
        r'person obtaining a copy of this software and(?:\/or)? '
        r'associated documentation files \(the[" ]*'
        r'(?:Software|Materials)[" ]?\), to deal in the '
        r'(?:Software|Materials)',
        'MIT/X11'),
    ReLicense(
        r'Permission to use, copy, modify, and distribute '
        r'(?:[^ ]+ ){,3}(?:and its (:?accompanying )?documentation )?'
        r'for any purpose and without fee is hereby granted',
        'MIT/X11'),
    ReLicense(
        r'Permission is hereby granted, without written agreement '
        r'and without license or royalty fees, to use, copy, '
        r'modify, and distribute this software and its '
        r'documentation for any purpose',
        'MIT/X11'),
    ReLicense(
        r'Permission is hereby granted, free of charge, to any member of '
        r'the KDE project \(the "?K Desktop Environment"? '
        r'https?[ :]/?/?www.kde.org\) obtaining a copy of this "?Konqi '
        r'SDK"? package and associated documentation files \(the '
        r'"?Package[ "]?\), to deal in the Package without restriction, '
        r'including without limitation the rights to use, copy, modify, '
        r'merge, publish, distribute, sublicense, and/or sell copies of '
        r'the Package, and to permit persons to whom the Software is '
        r'furnished to do so, subject to the following conditions:? '
        r'The above copyright notice and this permission notice shall be '
        r'included in all copies or substantial portions of the Package.',
        'MIT/X11',
        lambda *a: 'Expat-like-Carddecks'),
    # as used in kf5, mmh
    ReLicense(
        r'Distributed under terms of the MIT license\.',
        'MIT/X11',
        lambda *a: 'Expat'),
    ReLicense(
        r'Permission to use, copy, modify and distribute this software '
        r'and its documentation for any purpose and without fee is '
        r'hereby granted, provided that the above copyright notice '
        r'appears in all copies and that both that copyright notice '
        r'and this permission notice appear in supporting '
        r'documentation, and that the name of CMU and The Regents '
        r'of the University of California not be used in '
        r'advertising or publicity pertaining to distribution of '
        r'the software without specific written permission\.',
        'MIT/X11',
        lambda *a: 'MIT-CMU'),
    ReLicense(
        r'Permission to use, copy, modify, distribute, and sell '
        r'this software and its documentation for any purpose is '
        r'hereby granted without fee, provided that the above '
        r'copyright notice appear in all copies and that both that '
        r'copyright notice and this permission notice appear in '
        r'supporting documentation',
        'MIT/X11'),
    # as used by jquery
    ReLicense(
        r'Released under the MIT license '
        r'https?[ :]/?/?jquery.org[ /]license',
        'MIT/X11',
        lambda *a: 'Expat'),
    ReLicense(
        r'Permission to use, copy, modify, and distribute this software '
        r'for any purpose without fee is hereby granted, provided that '
        r'this entire notice is included in all copies of any software '
        r'which is or includes a copy or modification of this software '
        r'and in all copies of the supporting documentation for such '
        r'software.',
        'MIT/X11',
        lambda *a: 'ISC-like-dmgfp'),
    ReLicense(
        r'NASA OPEN SOURCE AGREEMENT VERSION ([\d.]+) '
        r'THIS OPEN SOURCE AGREEMENT \([" ]?AGREEMENT[" ]?\) DEFINES '
        r'THE RIGHTS OF USE, REPRODUCTION, DISTRIBUTION, MODIFICATION '
        r'AND REDISTRIBUTION OF CERTAIN COMPUTER SOFTWARE ORIGINALLY '
        r'RELEASED BY THE UNITED STATES GOVERNMENT AS REPRESENTED BY '
        r'THE GOVERNMENT AGENCY LISTED BELOW \([" ]?GOVERNMENT '
        r'AGENCY[" ]?\)\. '
        r'THE UNITED STATES GOVERNMENT, AS REPRESENTED BY GOVERNMENT '
        r'AGENCY, IS AN INTENDED THIRD-PARTY BENEFICIARY OF ALL '
        r'SUBSEQUENT DISTRIBUTIONS OR REDISTRIBUTIONS OF THE SUBJECT '
        r'SOFTWARE. ANYONE WHO USES, REPRODUCES, DISTRIBUTES, MODIFIES '
        r'OR REDISTRIBUTES THE SUBJECT SOFTWARE, AS DEFINED HEREIN, '
        r'OR ANY PART THEREOF, IS, BY THAT ACTION, ACCEPTING IN FULL '
        r'THE RESPONSIBILITIES AND OBLIGATIONS CONTAINED IN THIS '
        r'AGREEMENT\.',
        'NASA', _name_version),
    # Not even present in others license scanners. :/
    ReLicense(
        r'NAUMEN Public License '
        r'This software is '
        r'(?:[^\s]+ ){,16}'
        r'All rights reserved. '
        r'Redistribution and use in source and binary forms, with or '
        r'without modification, are permitted provided that the '
        r'following conditions are met',
        'Naumen'),
    # The BSD-3-clause and the Expat licenses had a baby license
    ReLicense(
        r'Permission is hereby granted, free of charge, to any person '
        r'obtaining a copy of this software and associated '
        r'documentation files \(the "?Software[" ]?\), to deal with '
        r'the Software without restriction, including without '
        r'limitation the rights to use, copy, modify, merge, publish, '
        r'distribute, sublicense, and/or sell copies of the Software, '
        r'and to permit persons to whom the Software is furnished to do '
        r'so, subject to the following conditions:? '
        r'.{,4}Redistributions of source code must retain the above '
        r'copyright notice, this list of conditions and the following '
        r'disclaimers\. '
        r'.{,4}Redistributions in binary form must reproduce the above '
        r'copyright notice, this list of conditions and the following '
        r'disclaimers in the documentation and/or other materials '
        r'provided with the distribution\. '
        r'.{,4}Neither the names of '
        r'(?:[^\s]+ ){,16}'
        r'nor the names of its contributors may be used to endorse or '
        r'promote products derived from this Software without specific '
        r'prior written permission\. '
        r'THE SOFTWARE IS PROVIDED "?AS IS[" ]?, WITHOUT WARRANTY OF '
        r'ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE '
        r'WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR '
        r'PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE '
        r'CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, '
        r'DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, '
        r'TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH '
        r'THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE\.',
        'NCSA'),
    ReLicense(
        r'Permission is granted to anyone to use this software for any '
        r'purpose on any computer system, and to redistribute it '
        r'freely, subject to the following restrictions:? '
        r'.{,4}This software is distributed in the hope that it will be '
        r'useful, but WITHOUT ANY WARRANTY;? without even the implied '
        r'warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR '
        r'PURPOSE\. '
        r'.{,4}Altered versions must be plainly marked as such, and must '
        r'not be misrepresented as being the original software\.',
        'Newsletr'),
    # Based on the original Bison GPL
    ReLicense(
        r'Everyone is permitted to copy and distribute verbatim copies '
        r'of this license, but changing it is not allowed. You can also '
        r'use this wording to make the terms for other programs\. '
        r'The license agreements of most software companies keep you '
        r'at the mercy of those companies\. By contrast, our general '
        r'public license is intended to give everyone the right to '
        r'share NetHack\. To make sure that you get the rights we want '
        r'you to have, we need to make restrictions that forbid anyone '
        r'to deny you these rights or to ask you to surrender the '
        r'rights\. Hence this license agreement\.',
        'NGPL'),
    ReLicense(
        r'This licence grants you the right to copy, use and '
        r'distribute information, provided you acknowledge the '
        r'contributors and comply with the terms and conditions '
        r'stipulated in this licence\. By using information made '
        r'available under this licence, you accept the terms and '
        r'conditions set forth in this licence\. As set out in Section '
        r'7, the licensor disclaims any and all liability for the '
        r'quality of the information and what the information is '
        r'used for\. '
        r'This licence shall not impose any limitations on the rights '
        r'or freedoms of the licensee under the Norwegian Freedom of '
        r'Information Act or any other legislation granting the general '
        r'public a right of access to public sector information, or '
        r'that follow from exemptions or limitations stipulated in '
        r'the Norwegian Copyright Act\. Further, the licence shall '
        r'not impose any limitations on the licensee\'s freedom of '
        r'expression recognized by law\.',
        'NLOD',
        lambda *a: 'NLOD-1'
    ),
    ReLicense(
        r'The contents of this file are subject to the NOKOS License '
        r'Version ([\d.]+) \(the "?License[" ]\);? you may not use this '
        r'file except in compliance with the License\. '
        r'Software distributed under the License is distributed on an '
        r'"?AS IS"? basis, WITHOUT WARRANTY OF ANY KIND, either express '
        r'or implied\. See the License for the specific language '
        r'governing rights and limitations under the License\.',
        'NOKOS', _name_version),
    ReLicense(
        r'The contents of this file are subject to the Netizen Open '
        r'Source License Version ([\d.]+) \(the "?License[" ]\);? '
        r'you may not use this file except in compliance with the '
        r'License\. You may obtain a copy of the License at '
        r'https?[: ]/?/?netizen.com.au/licenses/NOPL/? '
        r'Software distributed under the License is distributed on an '
        r'"?AS IS"? basis, WITHOUT WARRANTY OF ANY KIND, either express '
        r'or implied\. See the License for the specific language '
        r'governing rights and limitations under the License\.',
        'NOSL', _name_version),
    ReLicense(
        r'Noweb is available free for any use in any field of endeavor\. '
        r'You may redistribute noweb in whole or in part provided you '
        r'acknowledge its source and include this COPYRIGHT file\. You '
        r'may modify noweb and create derived works, provided you retain '
        r'this copyright notice, but the result may not be called noweb '
        r'without my written consent\.',
        'Noweb'),
    ReLicense(
        r'The contents of this file are subject to the Netscape Public '
        r'License Version ([\d.]+) \(the "?License[" ]\);? you may not '
        r'use this file except in compliance with the License\. You may '
        r'obtain a copy of the License at '
        r'https?[: ]/?/?www.mozilla.org/NPL/? '
        r'Software distributed under the License is distributed on an '
        r'"?AS IS"? basis, WITHOUT WARRANTY OF ANY KIND, either express '
        r'or implied\. See the License for the specific language '
        r'governing rights and limitations under the License\.',
        'MPL', partial(_name_version, name='NPL')),
    ReLicense(
        r'Licensed under the Non-Profit Open Software License version '
        r'([\d.]+)',
        'NPOSL', _name_version),
    ReLicense(
        r'NRL grants permission for redistribution and use in source '
        r'and binary forms, with or without modification, of the '
        r'software and documentation created at NRL provided that '
        r'the following conditions are met',
        'NRL'),
    ReLicense(
        r'Permission to use, copy, modify, and distribute this software '
        r'and its documentation for any purpose with or without fee is '
        r'hereby granted, provided that the above copyright notice '
        r'appears in all copies and that both the copyright notice and '
        r'this permission notice appear in supporting documentation, '
        r'and that the name '
        r'(?:[^\s]+ ){,16}'
        r'not be used in advertising or publicity pertaining to '
        r'distribution of the software without specific, written prior '
        r'permission\. '
        r'(?:[^\s]+ ){,16}'
        r'makes no representations about the suitability this software '
        r'for any purpose\. It is provided "?as is"? without express or '
        r'implied warranty\.',
        'NTP'),
    ReLicense(
        r'it may be copied and furnished to others, and derivative works '
        r'that comment on or otherwise explain it or assist in its '
        r'implementation may be prepared, copied, published, and '
        r'distributed, in whole or in part, without restriction of any '
        r'kind, provided that the above copyright notice and this section '
        r'are included on all such copies and derivative works. '
        r'However, this document itself may not be modified in any way, '
        r'including by removing the copyright notice or references to '
        r'OASIS, except as needed for the purpose of developing any '
        r'document or deliverable produced by an OASIS Technical '
        r'Committee \(in which case the rules applicable to copyrights, as '
        r'set forth in the OASIS IPR Policy, must be followed\) or as '
        r'required to translate it into languages other than English. '
        r'The limited permissions granted above are perpetual and will '
        r'not be revoked by OASIS or its successors or assigns.',
        'OASIS'),
    ReLicense(
        r'The content of this file is subject to the Open CASCADE '
        r'Technology Public License \(the "?License[" ]\)\. You may not '
        r'use the content of this file except in compliance with the '
        r'License\. Please obtain a copy of the License at '
        r'opencascade\.com and read it completely before using this '
        r'file\.',
        'OCCT-PL'),
    ReLicense(
        r'The contents of this file, as updated from time to time by the '
        r'OCLC Office of Research, are subject to OCLC Research Public '
        r'License Version ([\d.]+) \(the "?License[" ]\);? you may not '
        r'use this file except in compliance with the License\. You may '
        r'obtain a current copy of the License at '
        r'https?[: ]/?/?purl.oclc.org/oclc/research/ORPL/?\. Software '
        r'distributed under the License is distributed on an "?AS IS"? '
        r'basis, WITHOUT WARRANTY OF ANY KIND, either express or '
        r'implied\. See the License for the specific language governing '
        r'rights and limitations under the License\.',
        'OCLC', _name_version),
    ReLicense(
        r'The Open Database License \(ODbL\) is a license agreement '
        r'intended to allow users to freely share, modify, and use this '
        r'Database while maintaining this same freedom for others\. Many '
        r'databases are covered by copyright, and therefore this '
        r'document licenses these rights\. Some jurisdictions, mainly in '
        r'the European Union, have specific rights that cover databases, '
        r'and so the ODbL addresses these rights, too\. Finally, the '
        r'ODbL is also an agreement in contract for users of this '
        r'Database to act in certain ways in return for accessing this '
        r'Database\.',
        'ODbL',
        lambda *a: 'ODbL-1'
    ),
    ReLicense(
        r'SIL OPEN FONT LICENSE '
        r'Version ([\d.]+) -? ?(?:[^\s]+ ){,8}'
        r'PREAMBLE '
        r'The goals of the Open Font License \(OFL\) are to stimulate '
        r'worldwide development of (?:cooperative|collaborative) font '
        r'projects, to support the font creation efforts of academic '
        r'and linguistic communities, and to provide '
        r'(?:a free and open|an open) framework in which fonts may be '
        r'shared and improved in partnership with others\. '
        r'The OFL allows the licensed fonts to be used, studied, '
        r'modified and redistributed freely as long as they are not '
        r'sold by themselves\. The fonts, including any derivative '
        r'works, can be bundled, embedded, redistributed and(?:/or)? '
        r'sold with any software provided that '
        r'(?:the font names of derivative works are changed|'
        r'any reserved names are not used by derivative works)\. The '
        r'fonts and derivatives, however, cannot be released under any '
        r'other type of license\.',
        'OFL', _name_version),
    ReLicense(
        r'The OpenLDAP Public License '
        r'Version ([\d.]+),? '
        r'(?:[^\s]+ ){,32}'
        r'Redistribution and use of this software and associated '
        r'documentation \([" ]Software[" ]\), with or without '
        r'modification, are permitted provided that the following '
        r'conditions are met',
        'Apache', partial(_name_version, name='OLDAP')),
    ReLicense(
        r'Redistribution and use of this software and associated '
        r'documentation \([" ]Software[" ]\), with or without '
        r'modification, are permitted provided that the following '
        r'conditions are met:? '
        r'.{,4}Redistributions of source code must retain copyright '
        r'statements and notices\. Redistributions must also contain a '
        r'copy of this document\. '
        r'.{,4}Redistributions in binary form must reproduce the above '
        r'copyright notice, this list of conditions and the following '
        r'disclaimer in the documentation and/or other materials '
        r'provided with the distribution\. '
        r'.{,4}The names? (?:[^\s]+ ){,8}must not be used to endorse or '
        r'promote products derived from this Software without prior '
        r'written permission of (?:[^\s]+ ){,8}For written permission, '
        r'please contact (?:[^\s]+ ){,8}'
        r'.{,4}Products derived from this Software may not be called '
        r'(?:[^\s]+ ){,8}nor may (?:[^\s]+ ){,8}appear in their names '
        r'without prior written permission of (?:[^\s]+ ){,8}'
        r'(?:[^\s]+ ){,8}is a registered trademark of '
        r'(?:[^\s]+ ){,8}'
        r'.{,4}Due credit should be given to (?:[^\s]+ ){,8}'
        r'THIS SOFTWARE IS PROVIDED BY (?:[^\s]+ ){,8}AND CONTRIBUTORS '
        r'`?`?AS IS\'?\'? AND ANY EXPRESSED OR IMPLIED WARRANTIES, '
        r'INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF '
        r'MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE '
        r'DISCLAIMED. IN NO EVENT SHALL '
        r'(?:[^\s]+ ){,8}OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, '
        r'INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL '
        r'DAMAGES \(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF '
        r'SUBSTITUTE GOODS OR SERVICES;? LOSS OF USE, DATA, OR '
        r'PROFITS;? OR BUSINESS INTERRUPTION\) HOWEVER CAUSED AND ON '
        r'ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT '
        r'LIABILITY, OR TORT \(INCLUDING NEGLIGENCE OR OTHERWISE\) '
        r'ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF '
        r'ADVISED OF THE POSSIBILITY OF SUCH DAMAGE\.',
        'Apache',
        lambda *a: 'Plexus'
    ),
    ReLicense(
        r'permits you to use, copy, modify, distribute, and license '
        r'this Software and the Documentation for any purpose, provided '
        r'that existing copyright notices are retained in all copies '
        r'and that this notice is included verbatim in any '
        r'distributions\. No written agreement, license, or royalty fee '
        r'is required for any of the authorized uses. Modifications to '
        r'this Software and Documentation may be copyrighted by their '
        r'authors and need not follow the licensing terms described '
        r'here\. If modifications to this Software and Documentation '
        r'have new licensing terms, the new terms must be clearly '
        r'indicated on the first page of each file where they apply\.',
        'OML'),
    ReLicense(
        r'Redistribution and use in source and binary forms, with or without '
        r'modification, are permitted provided that the following conditions '
        r'are met:? '
        r'(?:[^\s]+ ){,2}'
        r'Redistributions of source code must retain the above copyright '
        r'notice, this list of conditions and the following disclaimer. '
        r'(?:[^\s]+ ){,2}'
        r'Redistributions in binary form must reproduce the above copyright '
        r'notice, this list of conditions and the following disclaimer in '
        r'the documentation and/or other materials provided with the '
        r'distribution. '
        r'(?:[^\s]+ ){,2}'
        r'All advertising materials mentioning features or use of this '
        r'software must display the following acknowledgment:? '
        r'"?This product includes software developed by the OpenSSL Project '
        r'for use in the OpenSSL Toolkit. \(https?[: ]/?/?www.openssl.org/\)"? '
        r'(?:[^\s]+ ){,2}'
        r'The names "?OpenSSL Toolkit"? and "?OpenSSL Project"? must not be used to '
        r'endorse or promote products derived from this software without '
        r'prior written permission. For written permission, please contact '
        r'(?:openssl-core|licensing)@openssl.org. '
        r'(?:[^\s]+ ){,2}'
        r'Products derived from this software may not be called "?OpenSSL"? '
        r'nor may "?OpenSSL"? appear in their names without prior written '
        r'permission of the OpenSSL Project. '
        r'(?:[^\s]+ ){,2}'
        r'Redistributions of any form whatsoever must retain the following '
        r'acknowledgment:? '
        r'"?This product includes software developed by the OpenSSL Project '
        r'for use in the OpenSSL Toolkit \(https?[: ]/?/?www.openssl.org/\)"?',
        'OpenSSL'),
    ReLicense(
        r'The module is,?(?: [^ ]+){,5} licensed under OpenSSL',
        'OpenSSL'),
    ReLicense(
        r'This submission to OpenSSL is to be made available under the '
        r'OpenSSL license, and only to the OpenSSL project, in order to '
        r'allow integration into the publicly distributed code. '
        r'The use of this code, or portions of this code, or concepts '
        r'embedded in this code, or modification of this code and/or '
        r'algorithm\(s\) in it, or the use of this code for any other '
        r'purpose than stated above, requires special licensing.',
        'OpenSSL'),
    ReLicense(
        r'Rights for redistribution and usage in source and binary forms '
        r'are granted according to the OpenSSL license.',
        'OpenSSL'),
    ReLicense(
        r'OPEN PUBLIC LICENSE '
        r'Version ([\d.]+) '
        r'[^\s]+ Definitions',
        'OPL', _name_version),
    ReLicense(
        r'This Source Code Form is subject to the terms of the OSET '
        r'Public License, v.([\d.]+) \([“" ]OSET-PL-[\d.]+[”" ]\)\. '
        r'If a copy of the OPL was not distributed with this file, You '
        r'can obtain one at:? www.OSETFoundation.org/public-license\.',
        'OSET-PL', _name_version),
    ReLicense(
        r'Licensed under the Open Software License version ([\d.]+)',
        'OSL', _name_version),
    ReLicense(
        r'The Open Data Commons -? ?Public Domain Dedication &? ?Licence '
        r'is a document intended to allow you to freely share, modify, '
        r'and use this work for any purpose and without any '
        r'restrictions\. This licence is intended for use on databases '
        r'or their contents \([" ]data[" ]\), either together or '
        r'individually\.',
        'PDDL',
        lambda *a: 'PDDL-1'
    ),
    ReLicense(
        r'is free (?:software|documentation); the (Free '
        r'Software Foundation|author[\( ]?s?\)?|copyright holders?) gives '
        r'unlimited permission to copy, '
        r'distribute and modify it\.',
        'Permissive',
        lambda *a: 'FSFUL'),
    ReLicense(
        r'is free (?:software|documentation); the (Free '
        r'Software Foundation|author[\( ]?s?\)?|copyright holders?) gives '
        r'unlimited permission to copy '
        r'and(?:/or)? distribute it, with or without modifications, '
        r'as long as this notice is preserved\.',
        'Permissive',
        lambda *a: 'FSFULLR'),
    ReLicense(
        r'Copying and distribution of this [^ ]+, with or without '
        r'modification, are permitted in any medium without royalty '
        r'provided the copyright notice and this notice are '
        r'preserved.',
        'Permissive',
        lambda *a: 'FSFAP'),
    ReLicense(
        r'Permission to use, copy, modify and distribute '
        r'this software and its documentation for any purpose '
        r'and without fee is hereby granted, provided that '
        r'the above copyright notice appear in all copies ?,? '
        r'(?:and )?that both (?:that )?(?:the )?copyright notice '
        r'and this permission notice appear in supporting '
        r'documentation(?: ?,? and that the name (?:of )?'
        r'(?:[^ ]+ ){,4}(?:or (?:[^ ]+ ){,8})?not be used in '
        r'advertising or publicity pertaining to distribution '
        r'of the software without specific, written prior '
        r'permission ?)?\.',
        'Permissive',
        lambda *a: 'HPND'),
    ReLicense(
        r'NO LIMIT PUBLIC LICENSE '
        r'Terms and conditions for copying, distribution, modification '
        r'or anything else\. '
        r'.{,4}No limit to do anything with this work and this license\.',
        'Permissive',
        lambda *a: 'NLPL'),
    ReLicense(
        r'This (?:[^ ]+ ){,2}may be copied and used freely without '
        r'restrictions\.',
        'Permissive'),
    ReLicense(
        r'Copying and distribution of this file, with or without modification, '
        r'is permitted in any medium without royalty provided the copyright '
        r'notice and this notice are preserved.',
        'Permissive'),
    ReLicense(
        r'The PHP License, version ([\d.]+) '
        r'(?:[^\s]+ ){,16}'
        r'Redistribution and use in source and binary forms, with or '
        r'without modification, is permitted provided that the '
        r'following conditions are met',
        'PHP', _name_version),
    ReLicense(
        r'This source file is subject to version ([^ ]+) of the '
        r'PHP license',
        'PHP', _name_version),
    ReLicense(
        r'Permission to use, copy, modify, and distribute this software '
        r'and its documentation for any purpose, without fee, and '
        r'without a written agreement is hereby granted, provided that '
        r'the above copyright notice and this paragraph and the '
        r'following two paragraphs appear in all copies\. '
        r'IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO '
        r'ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR '
        r'CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF '
        r'THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE '
        r'UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY '
        r'OF SUCH DAMAGE\. '
        r'THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY '
        r'WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED '
        r'WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR '
        r'PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "?AS IS"? '
        r'BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATIONS '
        r'TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR '
        r'MODIFICATIONS\.',
        'PostgreSQL'),
    ReLicense(
        r'This system is distributed in the hope that it will be '
        r'useful, but WITHOUT ANY WARRANTY;? without even the implied '
        r'warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR '
        r'PURPOSE\. Don\'t come complaining to us if you modify this '
        r'file and it doesn\'t work[!.]? If this file is modified by anyone '
        r'but the authors, those changes and their authors must be '
        r'explicitly stated HERE\.',
        'psfrag'),
    ReLicense(
        r'They may be copied and used for any purpose \(including '
        r'distribution as part of a for-profit product\), provided:? '
        r'.{,4}The original attribution of the programs is clearly '
        r'displayed in the product and/or documentation, even if the '
        r'programs are modified and/or renamed as part of the product\. '
        r'.{,4}The original source code of the programs is provided free '
        r'of charge \(except for reasonable distribution costs\). For a '
        r'definition of reasonable distribution costs, see the Gnu '
        r'General Public License or Larry Wall\'s Artistic License '
        r'\(provided with the Perl 4 kit\)\. The GPL and Artistic '
        r'License in NO WAY affect this license;? they are merely used '
        r'as examples of the spirit in which it is intended\. '
        r'.{,4}These programs are provided "?as-is[" ]\. No warranty or '
        r'guarantee of their fitness for any particular task is '
        r'provided\. Use of these programs is completely at your own '
        r'risk\. '
        r'Basically, I don\'t mind how you use the programs so long as '
        r'you acknowledge the author, and give people the originals if '
        r'they want them\.',
        'psutils'),
    ReLicense(
        r'This software includes Qhull from The Geometry Center\. Qhull '
        r'is copyrighted as noted above.\ Qhull is free software and may '
        r'be obtained via http from www\.qhull\.org\. It may be freely '
        r'copied, modified, and redistributed under the following '
        r'conditions',
        'Qhull'),
    ReLicense(
        r'You may copy this prolog in any way that is directly '
        r'related to this document. For other use of this prolog, '
        r'see your licensing agreement for Qt.',
        'QT_Prolog'),
    ReLicense(
        r'Rdisc \(this program\) was developed by Sun Microsystems, '
        r'Inc\. and is provided for unrestricted use provided that this '
        r'legend is included on all tape media and as a part of the '
        r'software program in whole or part. Users may copy or modify '
        r'Rdisc without charge, and they may freely distribute it\.',
        'Rdisc'),
    ReLicense(
        r'The contents of this file are subject to the Red Hat eCos '
        r'Public License Version ([\d.]+)',
        'RHeCos', _name_version),
    ReLicense(
        r'Part of the software embedded in this product is eCos -? ?'
        r'Embedded Configurable Operating System, a trademark of '
        r'Red Hat\.',
        'RHeCos'),
    ReLicense(
        r'Unless explicitly acquired and licensed from Licensor under '
        r'(?:the Technical Pursuit License \([" ]TPL[" ]\) Version [\d.]+ '
        r'or greater|another license), the contents of this file are '
        r'subject to the '
        r'Reciprocal Public License \([" ]RPL[" ]\) Version ([\d.]+), '
        r'or subsequent versions as allowed by the RPL, and You may '
        r'not copy or use this file in either source code or executable '
        r'form, except in compliance with the terms and conditions of '
        r'the RPL\.',
        'RPL', _name_version),
    ReLicense(
        r'The contents of this file, and the files included with this '
        r'file, are subject to the current version of the RealNetworks '
        r'Public Source License Version ([\d.]+) \(the "?RPSL[" ]\) ',
        'RPSL', _name_version),
    ReLicense(
        r'License to copy and use this software is granted provided '
        r'that it is identified as the "?RSA Data Security, Inc\. '
        r'([^\s]+ ){,8}Message-Digest Algorithm"? in all material '
        r'mentioning or referencing this software or this function\.',
        'RSA-MD'),
    ReLicense(
        r'The contents of this file are subject to the Ricoh Source Code '
        r'Public License Version ([\d.]+) \(the "?License[" ]\);? you '
        r'may not use this file except in compliance with the License\. '
        r'You may obtain a copy of the License at '
        r'https?[: ]/?/?www.risource.org/RPL',
        'RSCPL', _name_version),
    ReLicense(
        r'Licensed under the SCEA Shared Source License, Version '
        r'([\d.]+) \(the "?License[" ]\);? you may not use this file '
        r'except in compliance with the License\. You may obtain a copy '
        r'of the License at:? '
        r'https?[: ]/?/?research.scea.com/scea_shared_source_license.html',
        'SCEA', _name_version),
    ReLicense(
        r'Except to the extent portions of this file are made subject to '
        r'an alternative license as permitted in the SGI Free Software '
        r'License B, Version ([\d.]+) \(the "?License[" ]\), the '
        r'contents of this file are subject only to the provisions of '
        r'the License\. You may not use this file except in compliance '
        r'with the License\.',
        'SGI-B', _name_version),
    ReLicense(
        r'under the SGI Free Software License B',
        'SGI-B'),
    ReLicense(
        r'This Simple Public License ([\d.]+) \(SimPL [\d.]+ for short\) '
        r'is a plain language implementation of GPL [\d.]+. The words '
        r'are different, but the goal is the same -? ?to guarantee for '
        r'all users the freedom to share and change software\. If '
        r'anyone wonders about the meaning of the SimPL, they should '
        r'interpret it as consistent with GPL [\d.]+\.',
        'SimPL', _name_version),
    ReLicense(
        r'The contents of this file are subject to the Sun (?:Industry )?'
        r'Standards (?:Source )?License Version ([\d.]+) '
        r'\(the "?License[" ]?\)[;.]? '
        r'You may not use this file except in compliance with the '
        r'License\.',
        'SISSL', _name_version),
    # Also matching SFL
    ReLicense(
        r'The United States Government/Department of Defense/National '
        r'Security Agency/Office of Network Security '
        r'(?:\(collectively "?the U.S. Government[" ]\) )?'
        r'hereby grants permission '
        r'(?:to any person obtaining a copy'
        r'|for the copying and distribution of copies) '
        r'of the (?:[^ ]+ ){,4}source and object files'
        r'(?:,| \(the "?(?:[^ ]+ ){,4}Software[" ]\)) '
        r'and '
        r'(?:associated documentation files \(the "?'
        r'(?:[^ ]+ ){,4}Documentation[" ]\), or'
        r'|that of) '
        r'any (?:portions|part) thereof, '
        r'(?:to do the following, )?subject to the following '
        r'license conditions:? '
        r'.{,4}You may, (?:free of charge and )?without additional '
        r'permission '
        r'from the U\.S\. Government, '
        r'(?:use, copy, modify, sublicense and '
        r'otherwise )?distribute the (?:[^ ]+ ){,4}Software or '
        r'components of the (?:[^ ]+ ){,4}'
        r'Software, with or without (?:modifications|additions) '
        r'developed by you '
        r'(?:and/)?or by others(?: at no charge)?\. '
        r'.{,4}You may, (?:free of charge )?and without additional '
        r'permission '
        r'from the U\.S\. Government, distribute copies of the '
        r'(?:[^ ]+ ){,4}'
        r'Documentation, with or without (?:modifications|additions) '
        r'developed by '
        r'you (?:and/)?or by others,? at no charge or at a charge that '
        r'covers '
        r'the cost of reproducing (?:such|the) copies, provided that this '
        r'(?:[^ ]+ ){,4}'
        r'Public License is retained\. '
        r'.{,4}Furthermore, if you distribute the '
        r'(?:[^ ]+ ){,4}'
        r'Software or parts of '
        r'the SMP Software, with or without (?:modifications|additions) '
        r'developed by '
        r'you (?:and/)?or others, then you must either make available the '
        r'source (?:code )?to all portions of the '
        r'(?:[^ ]+ ){,4}Software \(exclusive '
        r'of any (?:modifications|additions) made by you (?:and/)?or '
        r'by others\) upon '
        r'request, or instead you may notify anyone requesting '
        r'(?:the SMP Software )?source (?:code )?that it is freely '
        r'available from '
        r'the U\.S\. Government\. '
        r'.{,4}(?:You may not omit )?Transmission of this '
        r'(?:[^ ]+ ){,4}License '
        r'(?:must accompany|agreement with) '
        r'whatever portions of the (?:[^ ]+ ){,4}Software '
        r'(?:you redistribute|that are distributed)\. '
        r'.{,4}(?:The (?:[^ ]+ ){,4}Software|Any users of this software '
        r'must be notified that it) '
        r'is (?:provided )?without warrant(?:y|ee) or guarantee '
        r'of any nature, express or implied, '
        r'(?:including without limitation '
        r'the warranties of merchantability and|nor is there any) '
        r'fitness for (?:a particular purpose|use represented)\. '
        r'.{,4}The U\.S\. Government cannot be held liable for any damages '
        r'either directly or indirectly caused by the use of the '
        r'(?:[^ ]+ ){,4}'
        r'Software\. '
        r'.{,4}It is not permitted to copy, sublicense, distribute or '
        r'transfer any of the (?:[^ ]+ ){,4}Software except as expressly '
        r'indicated herein\. Any attempts to do otherwise will be '
        r'considered a violation of this License and your rights to the '
        r'(?:[^ ]+ ){,4}will be voided\. ',
        'SMPPL'),
    ReLicense(
        r'The contents of this file are subject to the SNIA Public '
        r'License Version ([\d.]+) \(the "?License[" ]\);? you may not '
        r'use this file except in compliance with the License\. You '
        r'may obtain a copy of the License at:? ',
        'SNIA', _name_version),
    ReLicense(
        r'Permission is granted to anyone to use this software for any '
        r'purpose on any computer system, and to(?: alter it and)? '
        r'redistribute it(?: freely)?, subject to the following '
        r'restrictions:? '
        r'.{,4}The author is not responsible for the consequences of '
        r'use of this software, no matter how awful, even if they arise '
        r'from (?:defects|flaws) in it\. '
        r'.{,4}The origin of this software must not be misrepresented, '
        r'either by explicit claim or by omission\. '
        r'(Since few users ever read sources, credits must appear in '
        r'the documentation\. )?'
        r'.{,4}Altered versions must be plainly marked as such, and must '
        r'not be misrepresented as being the original software\. '
        r'(?:Since few users ever read sources, credits must appear in '
        r'the documentation\. )?'
        r'.{,4}(?:This notice may not be removed or altered\. )?',
        'Spencer',
        lambda t, m, l:
            '{}-{}'.format(l, '94' if m.group(1) else '86'),
    ),
    ReLicense(
        r'The contents of this file are subject to the Sun Public '
        r'License Version ([\d.]+) \(the "?License[" ]?\);? you may not use '
        r'this file except in compliance with the License\. ',
        'SPL', _name_version),
    ReLicense(
        r'The contents of this file are subject to the SugarCRM Public '
        r'License Version ([\d.]+) \((?:the )?[" ]?License[" ]?\);? '
        r'You may not use this file except in compliance with the '
        r'License\. ',
        'SugarCRM', _name_version),
    ReLicense(
        r'Sun RPC is a product of Sun Microsystems, Inc\. and is provided '
        r'for unrestricted use provided that this legend is included on all '
        r'tape media and as a part of the software program in whole or '
        r'part\. Users may copy or modify Sun RPC without charge, but are '
        r'not authorized to license or distribute it to anyone else except '
        r'as part of a product or program developed by the user or with the '
        r'express written consent of Sun Microsystems, Inc\. '
        r'SUN RPC IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING '
        r'THE WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A '
        r'PARTICULAR PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR '
        r'TRADE PRACTICE\.',
        'Sun-RPC'),
    ReLicense(
        r'The authors hereby grant permission to use, copy, modify, '
        r'distribute, and license this software and its documentation '
        r'for any purpose, provided that existing copyright notices '
        r'are retained in all copies and that this notice is included '
        r'verbatim in any distributions. No written agreement, license, '
        r'or royalty fee is required for any of the authorized uses\. '
        r'Modifications to this software may be copyrighted by their '
        r'authors and need not follow the licensing terms described '
        r'here, provided that the new terms are clearly indicated on '
        r'the first page of each file where they apply\. '
        r'IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO '
        r'ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR '
        r'CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE, '
        r'ITS DOCUMENTATION, OR ANY DERIVATIVES THEREOF, EVEN IF THE '
        r'AUTHORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. '
        r'THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY '
        r'WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED '
        r'WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR '
        r'PURPOSE, AND NON-INFRINGEMENT\. THIS SOFTWARE IS PROVIDED ON '
        r'AN "?AS IS"? BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE '
        r'NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, '
        r'ENHANCEMENTS, OR MODIFICATIONS\. '
        r'(?:[^ ]+ ){,256}'
        r'BY INSTALLING THIS SOFTWARE, YOU ACKNOWLEDGE THAT YOU HAVE '
        r'READ THIS AGREEMENT, THAT YOU UNDERSTAND IT, AND THAT YOU '
        r'AGREE TO BE BOUND BY ITS TERMS AND CONDITIONS\.',
        'TCL',
        lambda *a: 'SWL',
    ),
    ReLicense(
        r'The authors hereby grant permission to use, copy, modify, '
        r'distribute, and license this software and its documentation '
        r'for any purpose, provided that existing copyright notices '
        r'are retained in all copies and that this notice is included '
        r'verbatim in any distributions. No written agreement, license, '
        r'or royalty fee is required for any of the authorized uses\. '
        r'Modifications to this software may be copyrighted by their '
        r'authors and need not follow the licensing terms described '
        r'here, provided that the new terms are clearly indicated on '
        r'the first page of each file where they apply\. '
        r'IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO '
        r'ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR '
        r'CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE, '
        r'ITS DOCUMENTATION, OR ANY DERIVATIVES THEREOF, EVEN IF THE '
        r'AUTHORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. '
        r'THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY '
        r'WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED '
        r'WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR '
        r'PURPOSE, AND NON-INFRINGEMENT\. THIS SOFTWARE IS PROVIDED ON '
        r'AN "?AS IS"? BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE '
        r'NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, '
        r'ENHANCEMENTS, OR MODIFICATIONS\. ',
        'TCL'),
    ReLicense(
        r'TORQUE is a modification of OpenPBS which was developed by '
        r'NASA Ames Research Center, Lawrence Livermore National '
        r'Laboratory, and Veridian TORQUE Open Source License '
        r'v(?:ersion)? ?([\d.]+).? 1? ?Information Solutions, Inc\. ',
        'TORQUE', _name_version),
    # https://lists.debian.org/debian-legal/2006/01/msg00566.html
    ReLicense(
        r'Unicode, Inc\. hereby grants the right to freely use the '
        r'information supplied in this file in the creation of products '
        r'supporting the Unicode Standard, and to make copies of this '
        r'file in any form for internal or external distribution as long '
        r'as this notice remains attached\.',
        'Unicode'),
    ReLicense(
        r'Unicode Terms of Use '
        r'For the general privacy policy governing access to this site, '
        r'see the Unicode Privacy Policy. For trademark usage, see the '
        r'Unicode®? Consortium Name and Trademark Usage Policy\. ',
        'Unicode-TOU'),
    ReLicense(
        r'This is free and unencumbered software released into the '
        r'public domain\. '
        r'Anyone is free to copy, modify, publish, use, compile, sell, '
        r'or distribute this software, either in source code form or as '
        r'a compiled binary, for any purpose, commercial or '
        r'non-commercial, and by any means\. ',
        'Unlicense'),
    ReLicense(
        r'The above copyright notice and either this complete '
        r'permission notice or at a minimum a reference to the UPL must '
        r'be included in all copies or substantial portions of the '
        r'Software\. '
        r'THE SOFTWARE IS PROVIDED "?AS IS[" ]?, WITHOUT WARRANTY OF ANY '
        r'KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE '
        r'WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR '
        r'PURPOSE AND NONINFRINGEMENT\. IN NO EVENT SHALL THE AUTHORS OR '
        r'COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER '
        r'LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR '
        r'OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE '
        r'SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE\.',
        'UPL',
        lambda *a: 'UPL-1'),
    ReLicense(
        r'There are no restrictions on distributing unmodified copies '
        r'of Vim except that they must include this license text\. You '
        r'can also distribute unmodified parts of Vim, likewise '
        r'unrestricted except that they must include this license text\. '
        r'You are also allowed to include executables that you made from '
        r'the unmodified Vim sources, plus your own usage examples and '
        r'Vim scripts\.',
        'Vim'),
    ReLicense(
        r'Under this license, this Distribution may be modified and '
        r'the original version and modified versions may be copied, '
        r'distributed, publicly displayed and performed provided that '
        r'the following conditions are met:? '
        r'.{,4}Modified versions are distributed with source code and '
        r'documentation and with permission for others to use any code '
        r'and documentation \(whether in original or modified versions\) '
        r'as granted under this license[;.]? '
        r'.{,4}if modified, the source code, documentation, and user '
        r'run-time elements should be clearly labeled by placing an '
        r'identifier of origin \(such as a name, initial, or other tag\) '
        r'after the version number[;.]? '
        r'.{,4}users, modifiers, distributors, and others coming into '
        r'possession or using the Distribution in original or modified '
        r'form accept the entire risk as to the possession, use, and '
        r'performance of the Distribution[;.]? '
        r'.{,4}this copyright management information \(software '
        r'identifier and version number, copyright notice and '
        r'license\) shall be retained in all versions of the '
        r'Distribution[;.]? '
        r'.{,4}(?:[^ ]+ ){,4}may make modifications to the Distribution '
        r'that are substantially similar to modified versions of the '
        r'Distribution, and may make, use, sell, copy, distribute, '
        r'publicly display, and perform such modifications, including '
        r'making such modifications available under this or other '
        r'licenses, without obligation or restriction[;.]? '
        r'.{,4}modifications incorporating code, libraries, and/or '
        r'documentation subject to any other open source license may be '
        r'made, and the resulting work may be distributed under the '
        r'terms of such open source license if required by that open '
        r'source license, but doing so will not affect this '
        r'Distribution, other modifications made under this license or '
        r'modifications made under other (?:[^ ]+ ){,4}licensing '
        r'arrangements[;.]? '
        r'.{,4}no permission is granted to distribute, publicly '
        r'display, or publicly perform modifications to the '
        r'Distribution made using proprietary materials that cannot be '
        r'released in source format under conditions of this license[;.]?'
        r'.{,4}the name of (?:[^ ]+ ){,4}may not be used in advertising '
        r'or publicity pertaining to Distribution of the software '
        r'without specific, prior written permission[;.]? '
        r'This software is made available "?as is[" ]?, and '
        r'(?:[^ ]+ ){,4}DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, '
        r'WITH REGARD TO THIS SOFTWARE, INCLUDING WITHOUT LIMITATION ALL '
        r'IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A '
        r'PARTICULAR PURPOSE, AND IN NO EVENT SHALL VOSTROM BE LIABLE '
        r'FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY '
        r'DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR '
        r'PROFITS, WHETHER IN AN ACTION OF CONTRACT, TORT \(INCLUDING '
        r'NEGLIGENCE\) OR STRICT LIABILITY, ARISING OUT OF OR IN '
        r'CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE\.',
        'VOSTROM'),
    ReLicense(
        r'Redistribution and use in source and binary forms, with or '
        r'without modification, are permitted provided that the '
        r'following conditions are met:? '
        r'.{,4}Redistributions of source code must retain the above '
        r'copyright notice, this list of conditions and the following '
        r'disclaimer\. '
        r'.{,4}Redistributions in binary form must reproduce the above '
        r'copyright notice, this list of conditions and the following '
        r'disclaimer in the documentation and/or other materials '
        r'provided with the distribution\. '
        r'.{,4}The names (?:[^ ]+ ){,32}must not be used to endorse or '
        r'promote products derived from this software without prior '
        r'written permission. For written permission, please contact '
        r'(?:[^ ]+ ){,4}'
        r'.{,4}Products derived from this software may not be called '
        r'(?:[^ ]+ ){,4}nor may (?:[^ ]+ ){,4}appear in their name, '
        r'without prior written permission\. '
        r'THIS SOFTWARE IS PROVIDED "?AS IS"? AND ANY EXPRESSED OR '
        r'IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE '
        r'IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A '
        r'PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT ARE DISCLAIMED. '
        r'IN NO EVENT SHALL (?:[^ ]+ ){,8}OR ITS CONTRIBUTORS BE LIABLE '
        r'FOR ANY DAMAGES IN EXCESS OF (?:[^ ]+ ){,4}NOR FOR ANY '
        r'INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL '
        r'DAMAGES \(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF '
        r'SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; '
        r'OR BUSINESS INTERRUPTION\) HOWEVER CAUSED AND ON ANY THEORY OF '
        r'LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT '
        r'\(INCLUDING NEGLIGENCE OR OTHERWISE\) ARISING IN ANY WAY OUT '
        r'OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE '
        r'POSSIBILITY OF SUCH DAMAGE\.',
        'VSL',
        lambda *a: 'VSL-1'),
    ReLicense(
        r'By obtaining, using and/or copying this work, you \(the '
        r'licensee\) agree that you have read, understood, and will '
        r'comply with the following terms and conditions[:.]? '
        r'Permission to (?:use, )?copy, modify, and distribute this '
        r'software '
        r'and its documentation, with or without modification, for any '
        r'purpose and without fee or royalty is hereby granted, provided '
        r'that you include the following on ALL copies of the software '
        r'and documentation or portions thereof, including '
        r'modifications(?:, that you make)?[:.]? '
        r'.{,4}The full text of this NOTICE in a location viewable to '
        r'users of the redistributed or derivative work\. '
        r'.{,4}Any pre-existing intellectual property disclaimers, '
        r'notices, or terms and conditions. If none exist, '
        r'(?:the W3C Software|a) short '
        r'notice (?:should be included|of the following form) '
        r'\(hypertext is preferred, text '
        r'is permitted\) (?:should be used )?within the body of any '
        r'redistributed or derivative code[:.]? ("?Copyright ©? ?'
        r'\[?\$?date-of-software\]? World Wide Web Consortium, '
        r'\(Massachusetts Institute of Technology, Institut National de '
        r'Recherche en Informatique et en Automatique, Keio '
        r'University\)\. All Rights Reserved\. '
        r'https?[: ]/?/?www.w3.org/Consortium/Legal/?"? )?'
        r'.{,4}Notice of any changes or modifications to the '
        r'(?:W3C )?files, '
        r'including the date changes were made\. \(We recommend you '
        r'provide URIs to the location from which the code is derived\.\) '
        r'(?:Disclaimers )?'
        r'THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "?AS IS,"? AND '
        r'COPYRIGHT HOLDERS MAKE NO REPRESENTATIONS OR WARRANTIES, '
        r'EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO, WARRANTIES '
        r'OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR '
        r'THAT THE USE OF THE SOFTWARE OR DOCUMENTATION WILL NOT '
        r'INFRINGE ANY THIRD PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR '
        r'OTHER RIGHTS\. '
        r'COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, '
        r'SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE '
        r'SOFTWARE OR DOCUMENTATION\. '
        r'The name and trademarks of copyright holders may NOT be used '
        r'in advertising or publicity pertaining to the software without '
        r'specific, written prior permission. Title to copyright in this '
        r'software and any associated documentation will at all times '
        r'remain with copyright holders\. ',
        'W3C',
        lambda t, m, l:
            'W3C-19980720' if m.group(1) else l,
    ),
    ReLicense(
        r'This file contains Original Code and/or Modifications of '
        r'Original Code as defined in and that are subject to the '
        r'Sybase Open Watcom Public License version ([\d.]+) \(the '
        r'["\']?License[\'" ]?\)[.;] You may not use this file except '
        r'in compliance with the License\. BY USING THIS FILE YOU AGREE '
        r'TO ALL TERMS AND CONDITIONS OF THE LICENSE\. ',
        'Watcom', _name_version),
    ReLicense(
        r'Unlimited copying and redistribution of each of the files is '
        r'permitted as long as the file is not modified\. Modifications, '
        r'and redistribution of modified versions, are also permitted, '
        r'but only if the resulting file is renamed\. ',
        'Wsuipa'),
    ReLicense(
        r'Do What The Fuck You Want To Public License, Version '
        r'([^, ]+)',
        'WTFPL', _name_version),
    ReLicense(
        r'Do what The Fuck You Want To Public License',
        'WTFPL'),
    ReLicense(
        r'License WTFPL',
        'WTFPL'),
    ReLicense(
        r'Under (?:the|a) WTFPL',
        'WTFPL'),
    ReLicense(
        r'Use and copying of this software and preparation of derivative '
        r'works based upon this software are permitted\. Any copy of this '
        r'software or of any derivative work must include the above '
        r'copyright notice of Xerox Corporation, this paragraph and the '
        r'one after it\. Any distribution of this software or derivative '
        r'works must comply with all applicable United States export '
        r'control laws\. ',
        'Xerox'),
    ReLicense(
        r'The author \(Panagiotis Tsirigotis\) grants permission to use, '
        r'copy, and distribute this software and its documentation for '
        r'any purpose and without fee, provided that the above copyright '
        r'notice extant in files in this distribution is not removed '
        r'from files included in any redistribution and that this '
        r'copyright notice is also included in any redistribution\. ',
        'xinetd'),
    ReLicense(
        r'This product includes software developed by the Indiana '
        r'University Extreme!? Lab\. For further information please '
        r'visit https?[: ]/?/?www.extreme.indiana.edu/?',
        'xpp'),
    ReLicense(
        r'This program is free software;? you can redistribute it '
        r'freely\. '
        r'Use it at your own risk;? there is NO WARRANTY\. '
        r'Redistribution of modified versions is permitted provided that '
        r'the following conditions are met:? '
        r'.{,4}All copyright &? ?permission notices are preserved\. '
        r'.{,8}Only changes required for packaging or porting are made\. '
        r'or '
        r'.{,8}It is clearly stated who last changed the program\. The '
        r'program is renamed or the version number is of the form x.y.z, '
        r'where x.y is the version of the original program and z is an '
        r'arbitrary suffix\.',
        'XSkat'),
    ReLicense(
        r'Yahoo!? Public License, Version ([\d.]+) (?:\(YPL\) )?'
        r'This Yahoo!? Public License \(this "?Agreement[" ]\) is a '
        r'legal agreement that describes the terms under which Yahoo!? '
        r'Inc\., a Delaware corporation having its principal place of '
        r'business at 701 First Avenue, Sunnyvale, California 94089 '
        r'\([" ]Yahoo!?[" ]\) will provide software to you via download '
        r'or otherwise \([" ]Software[" ]\)\. By using the Software, '
        r'you, an individual or an entity \([" ]You[" ]\) agree to the '
        r'terms of this Agreement\.',
        'YPL', _name_version),
    ReLicense(
        r'You may copy and distribute this file freely. Any queries and '
        r'complaints should be forwarded to (?:[^ ]+ ){,4}'
        r'If you make any changes to this file, please do not distribute '
        r'the results under the name (?:[^ ]+ ){,4}',
        'Zed'),
    ReLicense(
        r'The Zend Engine License, version ([\d.]+) '
        r'(?:[^ ]+ ){,16}'
        r'Redistribution and use in source and binary forms, with or '
        r'without modification, is permitted provided that the following '
        r'conditions are met:? ',
        'Zend', _name_version),
    ReLicense(
        r'Zimbra Public License, Version ([\d.]+) (:?\(ZPL\) )?'
        r'This Zimbra Public License \(this ["“]?Agreement["” ]?\) is a '
        r'legal agreement that describes the terms under which '
        r'(?:VMware|Zimbra), Inc., a (?:Delaware|Texas) corporation ',
        'Zimbra', _name_version),
)

COMPILED_RES = tuple(
    re.compile(re_license.re, re_license.re_flags)
    for re_license in LICENSES_RES
)
