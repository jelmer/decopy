#!/bin/bash

coverage="false"

for i in "$@"; do
    case "$i" in
        --coverage|-c)
            coverage="true"
            shift
            ;;
        *)
            break
            ;;
    esac
done

declare -a CMD
if ${coverage}; then
    CMD=("python3-coverage" "run")
else
    CMD=("python3" "-B")
fi
# Args
CMD+=("-m" "unittest")

# Tests
CMD+=(
    "tests/dep5_test.py"
    "tests/matchers_test.py"
    "tests/matchers_spdx_test.py"
    "tests/tree_test.py"
    "tests/output_test.py"
)

"${CMD[@]}"

if ${coverage}; then
    python3-coverage report -m
fi
