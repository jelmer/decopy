# Installation

It's recommended that you use the debian package, but if you want to use this
outside of the package, you will need the following pieces:

## Required python modules
 * python3-xdg
 * python3-debian

## Required Software
 * exiftool: to handle the image files
 * bzcat: to handle the bzipped files
 * zcat: to handle the gzipped files
 * xzcat: to handle the xzipped files

## Recommended python modules
 * python3-tqdm: used for the --progress option
 * python3-regex: allows faster regular expression matching
